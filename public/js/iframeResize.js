function resizeIFrameToFitContent( iFrame ) {
  iFrame.width  = iFrame.contentWindow.document.body.scrollWidth;
  iFrame.height = iFrame.contentWindow.document.body.scrollHeight;
}

getFrames();

function getFrames(){
  var iframes = document.getElementsByClassName( 'kassuacms-iframe' );
  for( var i = 0; i < iframes.length; i++) {
    resizeIFrameToFitContent( iframes[i] );
  }
  
  setTimeout(getFrames, 200);
}
