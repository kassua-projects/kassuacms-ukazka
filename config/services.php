<?php

namespace Symfony\Component\DependencyInjection\Loader\Configurator;

return static function (ContainerConfigurator $containerConfigurator) {
    $services = $containerConfigurator->services();

    $services->load('Kassua\\CMSCore\\', '../src/')->autowire()->autoconfigure()
        ->exclude('../src/{DependencyInjection,Entity,Kernel.php}');
};
