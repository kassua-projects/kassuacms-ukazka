<?php

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return function (RoutingConfigurator $routes) {
    $routes->import('../src/Controller', 'attribute');

    $routes->add('kassua_cms.cms.dashboard', '/')
        ->controller([\Kassua\CMSCore\Controller\AdminController::class, 'dashboard']);
    $routes->add('kassua_cms.login', '/login')
        ->controller([\Kassua\CMSCore\Controller\LoginController::class, 'login']);
    $routes->add('kassua_cms.logout', '/logout');
    $routes->add('kassua_cms.cms.users.collection', '/users')
        ->controller([\Kassua\CMSCore\Controller\AdminUsersController::class, 'collection']);
    $routes->add('kassua_cms.cms.users.add', '/users/add')
        ->controller([\Kassua\CMSCore\Controller\AdminUsersController::class, 'add']);
    $routes->add('kassua_cms.cms.users.edit', '/users/edit/{id}')
        ->controller([\Kassua\CMSCore\Controller\AdminUsersController::class, 'edit']);
    $routes->add('kassua_cms.cms.users.profile', '/profile')
        ->controller([\Kassua\CMSCore\Controller\AdminUsersController::class, 'profile']);
};
