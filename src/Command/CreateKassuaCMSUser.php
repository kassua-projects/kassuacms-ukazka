<?php

namespace Kassua\CMSCore\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\MakerBundle\ConsoleStyle;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(name: 'kassua-cms:user:create-admin')]
class CreateKassuaCMSUser extends \Symfony\Component\Console\Command\Command
{
    public function __construct(private UserPasswordHasherInterface $passwordHasher, private UserRepository $userRepository)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new ConsoleStyle($input, $output);

        $mailContinue = false;
        while (!$mailContinue)
        {
            $mail = $io->ask('Type mail');
            $user = $this->userRepository->findOneBy(array('email' => $mail));
            if (!$user instanceof User && $mail !== null && trim($mail) !== '')
            {
                $mailContinue = true;
                $user = new User();
                $user->setEmail($mail);
            }
            else
            {
                $io->warning('User with mail '.$mail.' already exist.');
            }
        }

        $plainPassword = $io->ask('Type password for ' . $mail);
        $hashedPassword = $this->passwordHasher->hashPassword($user, $plainPassword);
        $user->setPassword($hashedPassword);
        $user->setRoles(array('ROLE_SUPERADMIN'));
        $this->userRepository->save($user, true);

        $io->success('User '.$mail.' created!');

        return Command::SUCCESS;
    }
}
