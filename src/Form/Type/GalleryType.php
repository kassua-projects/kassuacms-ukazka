<?php

namespace Kassua\CMSCore\Form\Type;

use Kassua\CMSCore\Model\ImageModel;
use Kassua\CMSCore\Structure\Gallery\GalleryStructure;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class GalleryType extends FileType
{
    private ?ImageModel $imageModel;

    public function __construct(TranslatorInterface $translator = null, ImageModel $imageModel = null)
    {
        $this->imageModel = $imageModel;
        parent::__construct($translator);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Ensure that submitted data is always an uploaded file or an array of some
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($options) {
            $form = $event->getForm();
            $requestHandler = $form->getConfig()->getRequestHandler();

            if ($options['multiple']) {
                $data = new GalleryStructure();
                $files = $event->getData();
//                dump($files);
                if (!\is_array($files)) {
                    $files = [];
                }

                foreach ($files as $file)
                {
                    $imageArray = json_decode($file, true);

                    if (!empty($imageArray['dataUrl']) && !empty($imageArray['file']['name']))
                    {
                        $base64 = explode( ';base64,', $imageArray['dataUrl']);
                        if (!empty($base64[1])) {
                            $base64String = $base64[1];

                            $image = $this->imageModel->saveImageFromString(
                                $imageArray['file']['name'],
                                base64_decode($base64String),
                                !empty($options['dirInUploads']) ? $options['dirInUploads'] : 'temp/'
                            );
                        } else {
                            $image = $this->imageModel->getImageStrucureFromPublicPath($imageArray['dataUrl']);
                        }

                        $data->addImage($image);
                    }
                }

//                $gallery = $this->getAdminService()->saveGalleryFromForm($files);
//                $data->setImages($gallery);

//                foreach ($files as $file) {
//                    if ($requestHandler->isFileUpload($file)) {
//                        dump($file);
//                        $data[] = $file;
//
//                        if (method_exists($requestHandler, 'getUploadFileError') && null !== $errorCode = $requestHandler->getUploadFileError($file)) {
//                            $form->addError($this->getFileUploadError($errorCode));
//                        }
//                    }
//                }

//                dump($data);
                // Since the array is never considered empty in the view data format
                // on submission, we need to evaluate the configured empty data here
//                if ([] === $data) {
//                    $emptyData = $form->getConfig()->getEmptyData();
//                    $data = $emptyData instanceof \Closure ? $emptyData($form, $data) : $emptyData;
//                }
//
                $event->setData($data);
            }
//            elseif ($requestHandler->isFileUpload($event->getData()) && method_exists($requestHandler, 'getUploadFileError') && null !== $errorCode = $requestHandler->getUploadFileError($event->getData())) {
//                $form->addError($this->getFileUploadError($errorCode));
//            } elseif (!$requestHandler->isFileUpload($event->getData())) {
//                $event->setData(null);
//            }
        });

//        parent::buildForm($builder, $options);
    }

    public function getBlockPrefix(): string
    {
        return 'gallery';
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if ($options['multiple']) {
            $view->vars['full_name'] .= '[]';
            $view->vars['attr']['multiple'] = 'multiple';
        }

        $galleryImages = array();

        /** @var GalleryStructure $gallery */
        $gallery = $form->getData();
        foreach ($gallery->getImages() as $galleryImage)
        {
            $galleryImages[] = array(
                'data_url' => $galleryImage->getPublicPath(),
                'file' => array(
                    'name' => $galleryImage->getFile()->getName()
                )
            );
        }

        $value = array(
            'images' => $galleryImages
        );

        $view->vars = array_replace($view->vars, [
            'type' => 'file',
            'value' => $value,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'dirInUploads' => 'temp/'
        ]);
        parent::configureOptions($resolver);
    }
}
