<?php

namespace Kassua\CMSCore\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AdminAbstractController
{
    public function dashboard(Request $request): Response
    {
        $this->setupAdminRoute('Dashboard', 'cms');

        return $this->renderAdmin('@KassuaCMSCore/dashboard/index.html.twig');
    }
}
