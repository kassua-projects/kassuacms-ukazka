<?php

namespace Kassua\CMSCore\Controller;

use App\Entity\User;
use App\Form\Type\UserEditType;
use App\Form\Type\UserType;
use App\Repository\UserRepository;
use Kassua\CMSCore\Entity\TypeEntity;
use Kassua\CMSCore\Model\DataTableModel;
use Kassua\CMSCore\Model\ObjectModel;
use Kassua\CMSCore\Service\AdminService;
use Kassua\CMSCore\Service\LayoutService;
use Kassua\CMSCore\Structure\User\UserStructure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminUsersController extends AdminAbstractController
{
    public function __construct(AdminService $adminService, LayoutService $layoutService)
    {
        parent::__construct($adminService, $layoutService);
        $this->setSecondaryMenuByType('cms');
    }

    private function getUserRepository(): UserRepository
    {
        return $this->getAdminService()->getUserRepository();
    }

    /**
     * Admin page with collection of users
     * @param Request $request
     * @param DataTableModel $dataTable
     * @return Response
     */
    public function collection(Request $request, DataTableModel $dataTable): Response
    {
        $this->setupAdminRoute('Uživatelé');

        $mailCol = $dataTable->addColumn('E-mail', 'mail', true);
        $actionCol = $dataTable->addColumn('Akce', 'action', false, 0, true);

        $users = $this->getUserRepository()->findAll();
        foreach ($users as $user)
        {
            $dataTable->addRow(array(
                //name
                $dataTable->getItemStructure(
                    $mailCol->getSelector(),
                    $user->getEmail()
                ),
                //action
                $dataTable->getActionItemStructure(
                    $actionCol->getSelector(),
                    $dataTable->getLinkContentStructure(
                        array(
                            $dataTable->getLinkContentItem(
                                $this->generateUrl('kassua_cms.cms.users.edit', ['id' => $user->getId()]),
                                'Upravit uživatele',
                                TypeEntity::EDIT
                            ),
                        )
                    )
                )
            ));
        }

        $this->getLayout()
            ->setDataTable($dataTable)
            ->addAddButton($this->generateUrl('kassua_cms.cms.users.add'), 'Přidat uživatele');

        return $this->renderCollection();
    }

    /**
     * @param UserStructure $entity
     * @param string|null $label
     * @return \Symfony\Component\Form\FormInterface
     */
    private function getForm(UserStructure $entity, string|null $label = null)
    {
        $options = array(
            'label' => $label
        );

        return $this->createForm(UserEditType::class, $entity, $options);
    }

    /**
     * Route pro přidání uživatele
     * @param Request $request
     * @param ObjectModel $objectModel
     * @return Response
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function add(Request $request, ObjectModel $objectModel): Response
    {
        $this->setupAdminRoute('Přidat uživatele');

        $form = $this->createForm(UserType::class, new UserStructure());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $entity = $this->getAdminService()->registerUser($form->getData());
            if (!$entity instanceof UserStructure)
                return $this->redirectToRoute('kassua_cms.cms.users.add');

            return $this->redirectToRoute('kassua_cms.cms.users.edit', ['id' => $entity->getId()]);
        }

        $objectModel->setForm($form);
        $this->getLayout()
            ->setContentObject($objectModel);

        return $this->renderObject();
    }

    /**
     * Route pro editaci uživatele
     * @param Request $request
     * @param int $id
     * @param ObjectModel $objectModel
     * @return Response
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function edit(Request $request, int $id, ObjectModel $objectModel): Response
    {
        $this->setupAdminRoute('Upravit uživatele');

        $user = $this->getUserRepository()->find($id);
        if (!$user instanceof User)
            return $this->redirectToRoute('kassua_cms.cms.users.collection');

        $entity = new UserStructure();
        $entity->setId($user->getId());
        $entity->setEmail($user->getEmail());
        $entity->setRoles($user->getRoles());

        $form = $this->getForm($entity, 'Upravit uživatele ' . $entity->getEmail());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $entity = $this->getAdminService()->saveUser($form->getData());
        }

        $objectModel->setForm($form);
        $this->getLayout()
            ->setContentObject($objectModel);

        return $this->renderObject();
    }

    /**
     * Stránka osobního profilu
     * @param Request $request
     * @param ObjectModel $objectModel
     * @return Response
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function profile(Request $request, ObjectModel $objectModel): Response
    {
        /** @var UserStructure $entity */
        $entity = $this->getUserRepository()->findBy(array('email' => $this->getUser()->getUserIdentifier()))[0];

        return $this->edit($request, $entity->getId(), $objectModel);
    }
}
