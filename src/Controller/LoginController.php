<?php

namespace Kassua\CMSCore\Controller;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends AdminAbstractController
{
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $this->setTitle('Přihlášení');

        if($this->getUser() instanceof UserInterface)
            return $this->redirectToRoute('kassua_cms.cms.dashboard');

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        $this->addContentProp('title', 'Přihlášení');
        $this->addContentProp('loginAction', $this->generateUrl('kassua_cms.login'));
        $this->addContentProp('error', $error instanceof AuthenticationException ? $error->getMessage() : '');
        $this->addContentProp('username', $lastUsername);

        return $this->renderAdmin('@KassuaCMSCore/login/index.html.twig');
    }
}
