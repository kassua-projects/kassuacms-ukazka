<?php

namespace Kassua\CMSCore\Controller\API;

use Kassua\CMSCore\Structure\API\APIStructure;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class AdminAbstractAPIController extends AbstractController
{
    public function apiResponse(string $content, int $status = 200, string $msg = '')
    {
        $apiStructure = new APIStructure();
        $apiStructure->setContent($content);
        $apiStructure->setStatus($status);
        $apiStructure->setMessage($msg);

        return new Response($apiStructure->toJSON());
    }
}
