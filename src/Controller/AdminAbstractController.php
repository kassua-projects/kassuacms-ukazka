<?php

namespace Kassua\CMSCore\Controller;

use Kassua\CMSCore\Model\DataTableModel;
use Kassua\CMSCore\Model\AdminModel;
use Kassua\CMSCore\Service\AdminService;
use Kassua\CMSCore\Service\LayoutService;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

class AdminAbstractController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    public function __construct(protected AdminService $adminService, protected LayoutService $layoutService)
    {
    }

    /**
     * Vrátí service pro práci s layoutem
     * a obsahem stránky
     * @return LayoutService
     */
    protected function getLayout()
    {
        return $this->layoutService;
    }

    /**
     * @return AdminService
     */
    protected function getAdminService()
    {
        return $this->adminService;
    }

    /**
     * @return AdminModel
     */
    protected function getAdminModel()
    {
        return $this->getAdminService()->getAdminModel();
    }

    /**
     * @param string $view
     * @param Response|null $response
     * @return Response
     */
    protected function renderCollection(string $view = '@KassuaCMSCore/collection/index.html.twig', Response $response = null): Response
    {
        return $this->renderAdmin($view, array(), $response);
    }

    /**
     * @param string $view
     * @param Response|null $response
     * @return Response
     */
    protected function renderObject(string $view = '@KassuaCMSCore/object/index.html.twig', Response $response = null): Response
    {
        return $this->renderAdmin($view, array(), $response);
    }

    /**
     * @param string $view
     * @param Response|null $response
     * @return Response
     */
    protected function renderToDo(string $view = '@KassuaCMSCore/todo/index.html.twig', Response $response = null): Response
    {
        return $this->renderAdmin($view, array(), $response);
    }

    /**
     * @param string $view
     * @param Response|null $response
     * @return Response
     */
    protected function renderAdmin(string $view, array $params = [], Response $response = null): Response
    {
        if (empty($params))
            $params = $this->getLayout()->createView();

        return $this->render($view, $params, $response);
    }

    /**
     * @param string $title
     * @return void
     */
    public function setTitle(string $title)
    {
        $this->getLayout()->getInterface()->setTitle($title);
    }

    /**
     * @param string $type
     * @return void
     */
    public function setSecondaryMenuByType(string $type)
    {
        $this->getLayout()->getInterface()->setSecondaryMenu($type);
    }

    /**
     * @param string $route
     * @return void
     */
    public function setActiveRoute(string $route)
    {
        $this->getLayout()->getInterface()->setActiveRoute($route);
    }

    /**
     * @param $key
     * @param $value
     * @return void
     */
    public function addContentProp($key, $value): void
    {
        $this->getLayout()->addContentProp($key, $value);
    }

    /**
     * @return RequestStack
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function getRequestStack(): RequestStack
    {
        if (!$this->container->has('request_stack'))
            throw new ServiceNotFoundException('request_stack');

        /** @var RequestStack $requestStack */
        $requestStack = $this->container->get('request_stack');

        return $requestStack;
    }

    /**
     * Nastaví základní nastavení layoutu a dané route.
     * @param string $title
     * @param string|null $secondaryMenuType
     * @param string|null $grantedForRole
     * @return void
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function setupAdminRoute(string $title, ?string $secondaryMenuType = null, ?string $grantedForRole = null): void
    {
        $request = $this->getRequestStack()->getCurrentRequest();

        if (!empty($grantedForRole))
            $this->denyAccessUnlessGranted($grantedForRole);
        else
            $this->denyAccessUnlessGrantedForCurrentRoute();

        $interface = $this->getLayout()->getInterface();
        $interface
            ->setActiveRoute($request->get('_route'))
            ->setTitle($title)
            ->setUserName($this->getAdminService()->getUserIdentifier())
            ->setLogoutLink($this->generateUrl('kassua_cms.logout'))
            ->setWebLink('/')
            ->setProfileLink($this->generateUrl('kassua_cms.cms.users.profile'));

        if (!empty($secondaryMenuType))
            $interface->setSecondaryMenu($secondaryMenuType);
    }

    /**
     * Zakáže vstup k route na základě konfigurace.
     * @return void
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function denyAccessUnlessGrantedForCurrentRoute(): void
    {
        $request = $this->getRequestStack()->getCurrentRequest();
        $grantedForRole = $this->getAdminService()->getRoleForRouteModel()->getRoleForRoute($request->get('_route'));
        if (!empty($grantedForRole))
            $this->denyAccessUnlessGranted($grantedForRole);
    }
}
