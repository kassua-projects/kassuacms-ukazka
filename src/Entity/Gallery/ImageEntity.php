<?php

namespace Kassua\CMSCore\Entity\Gallery;

use Kassua\CMSCore\Structure\FileStructure;
use Kassua\CMSCore\Structure\Gallery\ImageStructure;
use Symfony\Component\HttpFoundation\File\File;

class ImageEntity
{
    /** @var string $publicPath */
    public string $publicPath;

    /** @var string|null $alt */
    public string|null $alt;

    /** @var string|null $title */
    public string|null $title;

    /** @var string $fullPath */
    public string $fullPath;

    /** @var File $file */
    private File $file;

    /**
     * @return string
     */
    public function getPublicPath(): string
    {
        return $this->publicPath;
    }

    /**
     * @param string $publicPath
     */
    public function setPublicPath(string $publicPath): void
    {
        $this->publicPath = $publicPath;
    }

    /**
     * @return string|null
     */
    public function getAlt(): ?string
    {
        return $this->alt;
    }

    /**
     * @param string|null $alt
     */
    public function setAlt(?string $alt): void
    {
        $this->alt = $alt;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getFullPath(): string
    {
        return $this->fullPath;
    }

    /**
     * @param string $fullPath
     */
    public function setFullPath(string $fullPath): void
    {
        $this->fullPath = $fullPath;
    }

    /**
     * @return File
     */
    public function getFile(): File
    {
        if (empty($this->file) || !$this->file instanceof File)
            $this->file = new File($this->fullPath);

        return $this->file;
    }

    /**
     * @param File $file
     */
    public function setFile(File $file): void
    {
        $this->file = $file;

        $this->setFullPath($file->getPathname());
    }

    /**
     * @param ImageStructure $imageStructure
     * @return $this
     */
    public function fromStructure(ImageStructure $imageStructure)
    {
        $this->setTitle($imageStructure->getTitle());
        $this->setAlt($imageStructure->getAlt());
        $this->setPublicPath($imageStructure->getPublicPath());
        $this->setFullPath($imageStructure->getFile()->getPath());

        return $this;
    }

    /**
     * @param File $file
     * @return $this
     */
    public function fromFile(File $file, string $publicPath)
    {
        $this->setFile($file);
        $this->setPublicPath($publicPath);

        return $this;
    }

    /**
     * @param array $array
     * @return $this
     */
    public function fromArray(array $array): self|null
    {
        if (empty($array['publicPath']) || empty($array['fullPath'])) return null;

        $this->setPublicPath($array['publicPath']);
        $this->setFullPath($array['fullPath']);
        if (!empty($array['alt'])) $this->setAlt($array['alt']);
        if (!empty($array['title'])) $this->setTitle($array['fullPath']);

        return $this;
    }

    /**
     * @return ImageStructure
     */
    public function toStructure(): ImageStructure
    {
        $structure = new ImageStructure();
        $structure->setPublicPath($this->getPublicPath());
        if (!empty($this->title)) $structure->setTitle($this->getTitle());
        if (!empty($this->alt)) $structure->setAlt($this->getAlt());
        $structure->setFile(FileStructure::fromEntity($this->getFile()));

        return $structure;
    }

    public function toArray(): array
    {
        $array = array();
        $array['publicPath'] = $this->getPublicPath();
        $array['fullPath'] = $this->getFullPath();
        if (!empty($this->title)) $array['title'] = $this->getTitle();
        if (!empty($this->alt)) $array['alt'] = $this->getAlt();

        return $array;
    }
}
