<?php

namespace Kassua\CMSCore\Entity\Gallery;

use Kassua\CMSCore\Structure\Gallery\GalleryStructure;

class GalleryEntity
{
    /** @var ImageEntity[] $images */
    public array $images = array();

    /**
     * @return ImageEntity[]
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @param ImageEntity[] $images
     */
    public function setImages(array $images): void
    {
        $this->images = $images;
    }

    /**
     * @param ImageEntity $imageEntity
     * @return void
     */
    public function addImage(ImageEntity $imageEntity): void
    {
        $this->images[] = $imageEntity;
    }

    /**
     * @param GalleryStructure $structure
     * @return $this
     */
    public function fromStructure(GalleryStructure $structure): self
    {
        $images = array();
        foreach ($structure->getImages() as $image)
        {
            $imageEntity = new ImageEntity();
            $images[] = $imageEntity->fromStructure($image);
        }
        $this->setImages($images);

        return $this;
    }

    /**
     * @param array $array
     * @return $this
     */
    public function fromArray(array $array): self
    {
        if (!empty($array['images']))
        {
            $images = array();
            foreach ($array['images'] as $image)
            {
                $entity = new ImageEntity();
                $entity->fromArray($image);

                if ($entity instanceof ImageEntity)
                    $images[] = $entity;
            }
            $this->setImages($images);
        }

        return $this;
    }

    public function toArray(): array
    {
        $array = array();
        $array['images'] = array();
        foreach ($this->getImages() as $image)
        {
            $array['images'][] = $image->toArray();
        }

        return $array;
    }
}
