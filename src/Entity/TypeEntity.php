<?php

namespace Kassua\CMSCore\Entity;

class TypeEntity
{
    const EDIT = 'edit';
    const ADD = 'add';
    const REMOVE = 'remove';
    const UPDATE = 'update';
    const SAVE = 'save';

    private string $type;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }
}
