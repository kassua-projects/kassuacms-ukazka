<?php

namespace Kassua\CMSCore\Interface\DataTable;

interface ActionContentInterface
{
    public function getType();
    public function getData();
}
