<?php

namespace Kassua\CMSCore\Structure\Gallery;

use Kassua\CMSCore\Entity\Gallery\GalleryEntity;

class GalleryStructure
{
    /** @var ImageStructure[] $images */
    private array $images = array();

    /**
     * @return array
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @param array $images
     */
    public function setImages(array $images): void
    {
        $this->images = $images;
    }

    /**
     * @param ImageStructure $image
     * @return void
     */
    public function addImage(ImageStructure $image): void
    {
        $this->images[] = $image;
    }

    /**
     * @param GalleryEntity $entity
     * @return GalleryStructure
     */
    public static function fromEntity(GalleryEntity $entity): GalleryStructure
    {
        $structure = new GalleryStructure();
        foreach ($entity->getImages() as $image)
        {
            $structure->addImage(ImageStructure::fromEntity($image));
        }

        return $structure;
    }
}
