<?php

namespace Kassua\CMSCore\Structure\Gallery;

use Kassua\CMSCore\Entity\Gallery\ImageEntity;
use Kassua\CMSCore\Structure\FileStructure;
use Symfony\Component\HttpFoundation\File\File;

class ImageStructure
{
    /** @var string|null $publicPath */
    private string|null $publicPath;

    /** @var string|null $alt */
    private string|null $alt = null;

    /** @var string|null $title */
    private string|null $title = null;

    /** @var FileStructure $file */
    private FileStructure $file;

    /**
     * @return string|null
     */
    public function getPublicPath(): ?string
    {
        return $this->publicPath;
    }

    /**
     * @param string|null $publicPath
     */
    public function setPublicPath(?string $publicPath): void
    {
        $this->publicPath = $publicPath;
    }

    /**
     * @return string|null
     */
    public function getAlt(): ?string
    {
        return $this->alt;
    }

    /**
     * @param string|null $alt
     */
    public function setAlt(?string $alt): void
    {
        $this->alt = $alt;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return FileStructure
     */
    public function getFile(): FileStructure
    {
        return $this->file;
    }

    /**
     * @param FileStructure $file
     */
    public function setFile(FileStructure $file): void
    {
        $this->file = $file;
    }

    /**
     * @param ImageEntity $file
     * @return ImageStructure
     */
    public static function fromEntity(ImageEntity $image): ImageStructure
    {
        $structure = new ImageStructure();
        if (!empty($image->alt)) $structure->setAlt($image->getAlt());
        if (!empty($image->title)) $structure->setTitle($image->getTitle());
        $structure->setPublicPath($image->getPublicPath());
        $structure->setFile(FileStructure::fromEntity($image->getFile()));

        return $structure;
    }
}
