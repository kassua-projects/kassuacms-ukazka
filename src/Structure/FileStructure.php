<?php

namespace Kassua\CMSCore\Structure;

use Symfony\Component\HttpFoundation\File\File;

class FileStructure
{
    /** @var string $path */
    private string $path;

    /** @var string $name */
    private string $name;

    /** @var string $mimeType */
    private string $mimeType;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getMimeType(): string
    {
        return $this->mimeType;
    }

    /**
     * @param string $mimeType
     */
    public function setMimeType(string $mimeType): void
    {
        $this->mimeType = $mimeType;
    }

    /**
     * @param File $file
     * @return FileStructure
     */
    public static function fromEntity(File $file): FileStructure
    {
        $structure = new FileStructure();
        $structure->setName($file->getFilename());
        $structure->setPath($file->getPathname());
        $structure->setMimeType($file->getMimeType());

        return $structure;
    }
}
