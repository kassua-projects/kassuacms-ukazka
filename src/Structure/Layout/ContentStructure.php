<?php

namespace Kassua\CMSCore\Structure\Layout;

use Kassua\CMSCore\Model\DataTableModel;
use Kassua\CMSCore\Model\ObjectModel;

class ContentStructure
{
    private DataTableModel|null $dataTable = null;

    private ObjectModel|null $object = null;

    private array $contentProps = array();

    /**
     * @return DataTableModel|null
     */
    public function getDataTable(): ?DataTableModel
    {
        return $this->dataTable;
    }

    /**
     * @param DataTableModel|null $dataTable
     */
    public function setDataTable(?DataTableModel $dataTable): void
    {
        $this->dataTable = $dataTable;
    }

    /**
     * @return ObjectModel|null
     */
    public function getObject(): ?ObjectModel
    {
        return $this->object;
    }

    /**
     * @param ObjectModel|null $object
     */
    public function setObject(?ObjectModel $object): void
    {
        $this->object = $object;
    }

    /**
     * @return array
     */
    public function getContentProps(): array
    {
        return $this->contentProps;
    }

    /**
     * @param array $contentProps
     */
    public function setContentProps(array $contentProps): void
    {
        $this->contentProps = $contentProps;
    }

    /**
     * @param $key
     * @param $value
     * @return void
     */
    public function addContentProp($key, $value): void
    {
        $this->contentProps[$key] = $value;
    }
}
