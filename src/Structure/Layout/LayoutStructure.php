<?php

namespace Kassua\CMSCore\Structure\Layout;

class LayoutStructure
{
    /**
     * Props pro interface adminu
     * @var InterfaceStructure
     */
    private InterfaceStructure $interface;

    /**
     *  pro obsah konkrétní route. (object, collection ...)
     * @var ContentStructure
     */
    private ContentStructure $content;

    public function __construct()
    {
        $this->content = new ContentStructure();
        $this->interface = new InterfaceStructure();
    }

    /**
     * @return InterfaceStructure
     */
    public function getInterface(): InterfaceStructure
    {
        return $this->interface;
    }

    /**
     * @param InterfaceStructure $interface
     */
    public function setInterface(InterfaceStructure $interface): void
    {
        $this->interface = $interface;
    }

    /**
     * @return ContentStructure
     */
    public function getContent(): ContentStructure
    {
        return $this->content;
    }

    /**
     * @param ContentStructure $content
     */
    public function setContent(ContentStructure $content): void
    {
        $this->content = $content;
    }
}
