<?php

namespace Kassua\CMSCore\Structure\DataTable;

class ItemStructure
{
    private string $selector;
    private mixed $content;

    public function __construct($selector = null, $content = null)
    {
        if ($selector !== null) $this->setSelector($selector);
        if ($content !== null) $this->setContent($content);
    }

    /**
     * @return string
     */
    public function getSelector(): string
    {
        return $this->selector;
    }

    /**
     * @param string $selector
     */
    public function setSelector(string $selector): void
    {
        $this->selector = $selector;
    }

    /**
     * @return mixed
     */
    public function getContent(): mixed
    {
        return $this->content;
    }

    public function getContentArray(): mixed
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $return = array();

        $return[$this->getSelector()] = $this->getContentArray();

        return $return;
    }
}
