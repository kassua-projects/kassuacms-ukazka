<?php

namespace Kassua\CMSCore\Structure\DataTable;

class DataTableStructure
{
    private ColumnCollectionStructure $columnsCollection;
    private DataCollectionStrucure $dataCollection;
    private bool $selectableRows = false;

    /**
     * @return ColumnCollectionStructure
     */
    public function getColumnsCollection(): ColumnCollectionStructure
    {
        return $this->columnsCollection;
    }

    /**
     * @return array
     */
    public function getColumnsCollectionArray(): array
    {
        $return = array();

        /** @var ColumnStructure $column */
        foreach ($this->getColumnsCollection()->getColumns() as $column)
        {
            $return[] = $column->toArray();
        }

        return $return;
    }

    /**
     * @param ColumnCollectionStructure $columnsCollection
     */
    public function setColumnsCollection(ColumnCollectionStructure $columnsCollection): void
    {
        $this->columnsCollection = $columnsCollection;
    }

    /**
     * @return DataCollectionStrucure
     */
    public function getDataCollection(): DataCollectionStrucure
    {
        if (empty($this->dataCollection) || !$this->dataCollection instanceof DataCollectionStrucure)
            $this->dataCollection = new DataCollectionStrucure();

        return $this->dataCollection;
    }

    /**
     * @return array
     */
    public function getDataCollectionArray(): array
    {
        $return = array();

        /** @var DataStructure $dataStructure */
        foreach ($this->getDataCollection()->getData() as $dataStructure)
        {
            $return[] = $dataStructure->toArray();
        }

        return $return;
    }

    /**
     * @param DataCollectionStrucure $dataCollection
     */
    public function setDataCollection(DataCollectionStrucure $dataCollection): void
    {
        $this->dataCollection = $dataCollection;
    }

    /**
     * @return bool
     */
    public function isSelectableRows(): bool
    {
        return $this->selectableRows;
    }

    /**
     * @param bool $selectableRows
     */
    public function setSelectableRows(bool $selectableRows): void
    {
        $this->selectableRows = $selectableRows;
    }
}
