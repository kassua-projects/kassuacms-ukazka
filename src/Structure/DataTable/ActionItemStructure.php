<?php

namespace Kassua\CMSCore\Structure\DataTable;

class ActionItemStructure extends ItemStructure
{
    public function __construct($selector = null)
    {
        parent::__construct($selector);
    }

    /**
     * @param mixed $contentStructure
     * @return void
     */
    public function setContent(mixed $contentStructure): void
    {
        if (!$contentStructure instanceof \Kassua\CMSCore\Interface\DataTable\ActionContentInterface)
        {
            throw new \Exception('Unallowed content structure: ' . gettype($contentStructure));
        }

        parent::setContent($contentStructure);
    }

    public function getContentArray(): mixed
    {
        /** @var \Kassua\CMSCore\Interface\ActionContentInterface $content */
        $content = $this->getContent();
        return array(
            'type' => $content->getType(),
            'data' => $content->getData()
        );
    }
}
