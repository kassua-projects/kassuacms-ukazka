<?php

namespace Kassua\CMSCore\Structure\DataTable;

class ColumnStructure
{
    /** @var string $name */
    private $name;

    /** @var string $selector */
    private $selector;

    /** @var bool $sortable */
    private $sortable = false;

    /** @var integer $grow */
    private $grow = 1;

    /** @var bool $button */
    private $button = false;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSelector(): string
    {
        return $this->selector;
    }

    /**
     * @param string $selector
     */
    public function setSelector(string $selector): void
    {
        $this->selector = $selector;
    }

    /**
     * @return bool
     */
    public function isSortable(): bool
    {
        return $this->sortable;
    }

    /**
     * @param bool $sortable
     */
    public function setSortable(bool $sortable): void
    {
        $this->sortable = $sortable;
    }

    /**
     * @return int
     */
    public function getGrow(): int
    {
        return $this->grow;
    }

    /**
     * @param int $grow
     */
    public function setGrow(int $grow): void
    {
        $this->grow = $grow;
    }

    /**
     * @return bool
     */
    public function isButton(): bool
    {
        return $this->button;
    }

    /**
     * @param bool $button
     */
    public function setButton(bool $button): void
    {
        $this->button = $button;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = array();

        $data['name'] = $this->getName();
        $data['selector'] = $this->getSelector();
        $data['sortable'] = $this->isSortable();
        $data['grow'] = $this->getGrow();
        $data['button'] = $this->isButton();

        return $data;
    }
}
