<?php

namespace Kassua\CMSCore\Structure\DataTable;

class DataStructure
{
    /** @var ItemStructure[] $items */
    private $items;

    /**
     * @return ItemStructure[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param ItemStructure[] $items
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    /**
     * @param ItemStructure $item
     * @return void
     */
    public function addItem(ItemStructure $item): void
    {
        $this->items[] = $item;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = array();

        /** @var ItemStructure $item */
        foreach ($this->getItems() as $item)
        {
            $data = array_merge($data, $item->toArray());
        }

        return $data;
    }
}
