<?php

namespace Kassua\CMSCore\Structure\DataTable;

class LinkContentStructure implements \Kassua\CMSCore\Interface\DataTable\ActionContentInterface
{
    const EDIT_TYPE = 'edit';
    const REMOVE_TYPE = 'remove';

    private array $data = array();

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'link';
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    public function addItem($href, $title, $type, $target = '_self'): void
    {
        $this->data[] = array(
            'href' => $href,
            'title' => $title,
            'type' => $type,
            'target' => $target
        );
    }
}
