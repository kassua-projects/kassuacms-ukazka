<?php

namespace Kassua\CMSCore\Structure\DataTable;

class DataCollectionStrucure
{
    /** @var DataStructure[] $data */
    private $dataCollection = array();

    /**
     * @return DataStructure[]
     */
    public function getData()
    {
        return $this->dataCollection;
    }

    /**
     * @param DataStructure[] $dataCollection
     */
    public function setData($dataCollection): void
    {
        $this->dataCollection = $dataCollection;
    }

    /**
     * @param DataStructure $dataStructure
     * @return void
     */
    public function addData(DataStructure $dataStructure)
    {
        $this->dataCollection[] = $dataStructure;
    }
}
