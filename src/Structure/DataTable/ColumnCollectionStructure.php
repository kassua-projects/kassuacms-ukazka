<?php

namespace Kassua\CMSCore\Structure\DataTable;

class ColumnCollectionStructure
{
    /** @var ColumnStructure[] $columns */
    private $columns = array();

    /**
     * @param ColumnStructure $column
     * @return $this
     */
    public function addColumn(ColumnStructure $column)
    {
        $this->columns[] = $column;

        return $this;
    }

    /**
     * @return ColumnStructure[]
     */
    public function getColumns(): array
    {
        return $this->columns;
    }
}
