<?php

namespace Kassua\CMSCore\Structure\Object;

use Symfony\Component\Form\FormInterface;

class ObjectStructure
{
    /** @var FormInterface $symfonyForm */
    private $symfonyForm = null;

    /**
     * @return FormInterface|null
     */
    public function getSymfonyForm(): ?FormInterface
    {
        return $this->symfonyForm;
    }

    /**
     * @param FormInterface|null $symfonyForm
     */
    public function setSymfonyForm(?FormInterface $symfonyForm): void
    {
        $this->symfonyForm = $symfonyForm;
    }
}
