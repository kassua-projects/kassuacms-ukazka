<?php

namespace Kassua\CMSCore\Structure\AdminFrontend;

class AdminPropsStructure
{
    /**
     * @var MenuStructure|null $primaryMenu
     */
    private $primaryMenu = null;

    /**
     * @var MenuStructure|null $secondaryMenu
     */
    private $secondaryMenu = null;

    /**
     * @var string $title
     */
    private string $title = '';

    /**
     * @var string $activeRoute
     */
    private string $activeRoute = '';

    /** @var string $userName */
    private string $userName = '';

    private string $logoutLink = '';

    private string $profileLink = '';

    private string $webLink = '';

    /**
     * @return MenuStructure|null
     */
    public function getPrimaryMenu()
    {
        return $this->primaryMenu;
    }

    /**
     * @param MenuStructure $primaryMenu
     */
    public function setPrimaryMenu(?MenuStructure $primaryMenu): void
    {
        $this->primaryMenu = $primaryMenu;
    }

    /**
     * @return MenuStructure|null
     */
    public function getSecondaryMenu()
    {
        return $this->secondaryMenu;
    }

    /**
     * @param MenuStructure $secondaryMenu
     */
    public function setSecondaryMenu(?MenuStructure $secondaryMenu): void
    {
        $this->secondaryMenu = $secondaryMenu;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getActiveRoute(): string
    {
        return $this->activeRoute;
    }

    /**
     * @param string $activeRoute
     */
    public function setActiveRoute(string $activeRoute): void
    {
        $this->activeRoute = $activeRoute;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     */
    public function setUserName(string $userName): void
    {
        $this->userName = $userName;
    }

    /**
     * @return string
     */
    public function getLogoutLink(): string
    {
        return $this->logoutLink;
    }

    /**
     * @param string $logoutLink
     */
    public function setLogoutLink(string $logoutLink): void
    {
        $this->logoutLink = $logoutLink;
    }

    /**
     * @return string
     */
    public function getProfileLink(): string
    {
        return $this->profileLink;
    }

    /**
     * @param string $profileLink
     */
    public function setProfileLink(string $profileLink): void
    {
        $this->profileLink = $profileLink;
    }

    /**
     * @return string
     */
    public function getWebLink(): string
    {
        return $this->webLink;
    }

    /**
     * @param string $webLink
     */
    public function setWebLink(string $webLink): void
    {
        $this->webLink = $webLink;
    }
}
