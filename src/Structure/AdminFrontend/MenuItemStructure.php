<?php

namespace Kassua\CMSCore\Structure\AdminFrontend;

class MenuItemStructure
{
    /**
     * @var string $url
     */
    private string $url;

    /**
     * @var string $route
     */
    private string $route;

    /**
     * @var string $title
     */
    private string $title;

    /** @var string|null $menuType */
    private ?string $menuType = null;

    /**
     * @var bool $isActive
     */
    private bool $isActive = false;

    /**
     * @var string|null $icon
     */
    private ?string $icon = null;

    /**
     * @var string|null $roleForAccess
     */
    private ?string $roleForAccess = null;

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * @param string $route
     */
    public function setRoute(string $route): void
    {
        $this->route = $route;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @param string|null $icon
     */
    public function setIcon(?string $icon): void
    {
        $this->icon = $icon;
    }

    /**
     * @return string|null
     */
    public function getMenuType(): ?string
    {
        return $this->menuType;
    }

    /**
     * @param string|null $menuType
     */
    public function setMenuType(?string $menuType): void
    {
        $this->menuType = $menuType;
    }

    /**
     * @return string|null
     */
    public function getRoleForAccess(): ?string
    {
        return $this->roleForAccess;
    }

    /**
     * @param string|null $roleForAccess
     */
    public function setRoleForAccess(?string $roleForAccess): void
    {
        $this->roleForAccess = $roleForAccess;
    }

    /**
     * @param array $item
     * @return MenuItemStructure
     */
    public static function fromArray(array $item): MenuItemStructure
    {
        $itemStructure = new MenuItemStructure();
        $itemStructure->setRoute($item['route']);
        $itemStructure->setTitle($item['title']);
        if (!empty($item['menuType'])) $itemStructure->setMenuType($item['menuType']);
        if (!empty($item['icon'])) $itemStructure->setIcon($item['icon']);
        if (!empty($item['roleForAccess'])) $itemStructure->setRoleForAccess($item['roleForAccess']);

        return $itemStructure;
    }
}
