<?php

namespace Kassua\CMSCore\Structure\AdminFrontend;

class MenuStructure
{
    /**
     * @var string $type
     */
    private string $type;

    /** @var string|null $title */
    private ?string $title = null;

    /**
     * @var MenuItemStructure[] $items
     */
    private $items;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return MenuItemStructure[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return ?string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @param MenuItemStructure[] $items
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    /**
     * @param MenuItemStructure $item
     * @return void
     */
    public function addItem(MenuItemStructure $item): void
    {
        $this->items[] = $item;
    }
}
