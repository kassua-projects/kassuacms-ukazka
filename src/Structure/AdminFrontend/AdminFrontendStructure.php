<?php

namespace Kassua\CMSCore\Structure\AdminFrontend;

class AdminFrontendStructure
{
    /**
     * @var AdminPropsStructure
     * Props for admin layout etc.
     */
    private AdminPropsStructure $adminProps;

    /**
     * @var BackendPropsStructure
     * Props for content section (collection, object, dashboard)
     */
    private BackendPropsStructure $backendProps;

}
