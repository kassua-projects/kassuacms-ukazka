<?php

namespace Kassua\CMSCore\Model;

use Kassua\CMSCore\Structure\Gallery\GalleryStructure;
use Kassua\CMSCore\Structure\Object\Field\TextFieldStructure;
use Kassua\CMSCore\Structure\Object\ObjectStructure;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Contracts\Service\Attribute\Required;
use Symfony\Contracts\Service\ServiceSubscriberInterface;

class LayoutObjectModel implements ServiceSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /** @var ObjectStructure $objectStructure */
    private $objectStructure;

    /**
     * @required
     */
    #[Required]
    public function setContainer(ContainerInterface $container): ?ContainerInterface
    {
        $previous = $this->container;
        $this->container = $container;

        return $previous;
    }

    public static function getSubscribedServices(): array
    {
        return [
            'parameter_bag' => '?'.ContainerBagInterface::class,
        ];
    }

    /**
     * @return ObjectStructure
     */
    public function getObjectStructure(): ObjectStructure
    {
        if (!$this->objectStructure instanceof ObjectStructure)
            $this->objectStructure = new ObjectStructure();

        return $this->objectStructure;
    }

    /**
     * @param ObjectStructure $objectStructure
     */
    public function setObjectStructure(ObjectStructure $objectStructure): void
    {
        $this->objectStructure = $objectStructure;
    }

    /**
     * @param FormInterface $form
     * @return void
     */
    public function setForm(FormInterface $form): void
    {
        $object = $this->getObjectStructure()->setSymfonyForm($form);
    }

    /**
     * @return array
     */
    public function returnParamsForFrontend()
    {
        $object = $this->getObjectStructure();

        if (!$object->getSymfonyForm() instanceof FormInterface)
            return array();

        $formView = $object->getSymfonyForm()->createView();
//        $formVars = $formView->vars;

//        $fields = array();
//        dump($formView);
//        foreach ($formView->children as $field)
//        {
//            $vars = $field->vars;
//            dump($vars);
//            $fields[] = $this->getFieldArrayForFrontendFromVars($vars);
//        }

//        $data = array(
//            'form' => array(
//                'name' => $formVars['full_name'],
//                'method' => $formVars['method'],
//                'action' => $formVars['action'],
//                'fields' => $fields,
//                'label' => $formVars['label'],
//            )
//        );
//        $data = (array) $object->getSymfonyForm()->createView();
//        $data = array(
//            'form' => array(
//                'vars' => array(
//                    'full_name' => $formVars['full_name'],
//                    'name' => $formVars['name'],
//                    'method' => $formVars['method'],
//                    'action' => $formVars['action'],
//                    'label' => $formVars['label'],
//                    'block_prefixes' => $formVars['block_prefixes'],
//                ),
//                'children' => array(
//
//                ),
//            ),
//        );
//        dump($data);
        $data = array(
            'form' => $this->getFormForFrontendFromView($formView)
        );
//        dump($formView);
        return $data;
    }

    public function getFormForFrontendFromView(FormView $formView)
    {
//        dump($formView);
//        exit();
        $children = array();
        foreach ($formView->children as $child)
        {
            $children[] = $this->getFormForFrontendFromView($child);
        }

        $formVars = $formView->vars;
        $vars = array(
            'full_name' => $formVars['block_prefixes'][1] == 'gallery' ? substr($formVars['full_name'], 0, -2) : $formVars['full_name'],
            'name' => $formVars['name'],
            'label' => $formVars['label'],
            'block_prefixes' => $formVars['block_prefixes'],
        );
        if (isset($formVars['attr'])) $vars['attr'] = $formVars['attr'];
        if (isset($formVars['method'])) $vars['method'] = $formVars['method'];
        if (isset($formVars['action'])) $vars['action'] = $formVars['action'];
        if (isset($formVars['value'])) $vars['value'] = $formVars['value'];
        if (isset($formVars['required'])) $vars['required'] = $formVars['required'];
        if (isset($formVars['choices'])) $vars['choices'] = $formVars['choices'];
        if (isset($formVars['multiple'])) $vars['multiple'] = $formVars['multiple'];
        if (isset($formVars['prototype'])) $vars['prototype'] = $this->getFormForFrontendFromView($formVars['prototype']);

        $data = array(
            'vars' => $vars,
            'children' => $children,
        );

        return $data;
    }

    /**
     * @param array $vars
     * @param int $dep
     * @return array
     */
    private function getFieldArrayForFrontendFromVars(array $vars, int $dep = 0)
    {
//        dump($vars);
        $type = $vars['block_prefixes'][1];
        if (!empty($vars['block_prefixes'][2]) && $vars['block_prefixes'][1] === 'text' && $vars['block_prefixes'][2] === 'textarea')
        {
            $dep++;
            $type = $vars['block_prefixes'][2];
        }

        $prototype = array();
        $children = array();
        if (!empty($vars['prototype']))
        {
            $prototype = $this->getFieldArrayForFrontendFromVars($vars['prototype']->vars, $dep + 1);
            foreach ($vars['prototype']->children as $child)
            {
//                dump($child);
                $children[] = $this->getFieldArrayForFrontendFromVars($child->vars, $dep + 1);
            }
        }

        $choices = array();
        if (!empty($vars['choices']))
        {
            foreach ($vars['choices'] as $choice)
            {
                $choices[] = array(
                    'label' => $choice->label,
                    'value' => $choice->value
                );
            }
        }

        $name = $vars['full_name'];
        $multiple = false;
        if (!empty($vars['multipart']))
        {
            $multiple = $vars['multipart'];
            $name = substr($name, 0, -2);
        }

        $field = array(
            'type' => $type,
            'name' => $name,
            'label' => $vars['label'],
            'value' => $vars['value'],
            'required' => !empty($vars['required']) ? $vars['required'] : false,
            'prototype' => $prototype,
            'children' => $children,
            'choices' => $choices,
            'multiple' => $multiple
        );

        return $field;
    }
}
