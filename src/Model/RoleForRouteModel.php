<?php

namespace Kassua\CMSCore\Model;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class RoleForRouteModel
{
    /**
     * Všechny route s danou přístupovou rolí
     * Klíč route, value role
     * @var array
     */
    private array $roleForRouteCollection = array();

    public function __construct(protected ParameterBagInterface $parameterBag)
    {
    }

    /**
     * Najde pro roli, která povolí přístup k dané route
     * @param string $route
     * @return string|null
     */
    public function getRoleForRoute(string $route)
    {
        $roleForRoute = $this->getRoleForRouteCollectionFromConfig();
        if (!empty($roleForRoute[$route]))
            return $roleForRoute[$route];

        return null;
    }

    public function getRoleForRouteCollectionFromConfig()
    {
        if (empty($this->roleForRouteCollection) && $this->parameterBag->has('kassua_cms_core.roleForRoute'))
        {
            $roleForRoute = $this->parameterBag->get('kassua_cms_core.roleForRoute');
            foreach ($roleForRoute as $roleForRouteSingle) {
                $this->roleForRouteCollection[$roleForRouteSingle['route']] = $roleForRouteSingle['role'];
            }
        }

        return $this->roleForRouteCollection;
    }

}
