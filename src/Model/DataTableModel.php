<?php

namespace Kassua\CMSCore\Model;

use Kassua\CMSCore\Interface\DataTable\ActionContentInterface;
use Kassua\CMSCore\Structure\DataTable\ActionItemStructure;
use Kassua\CMSCore\Structure\DataTable\ColumnCollectionStructure;
use Kassua\CMSCore\Structure\DataTable\ColumnStructure;
use Kassua\CMSCore\Structure\DataTable\DataCollectionStrucure;
use Kassua\CMSCore\Structure\DataTable\DataStructure;
use Kassua\CMSCore\Structure\DataTable\DataTableStructure;
use Kassua\CMSCore\Structure\DataTable\ItemStructure;
use Kassua\CMSCore\Structure\DataTable\LinkContentStructure;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Contracts\Service\Attribute\Required;
use Symfony\Contracts\Service\ServiceSubscriberInterface;

class DataTableModel implements ServiceSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /** @var DataTableStructure|null $dataTableStructure */
    private $dataTableStructure = null;

    /** @var ColumnCollectionStructure $columnCollectionStructure */
    private ColumnCollectionStructure $columnCollectionStructure;

    /** @var DataCollectionStrucure $dataCollectionStrucure */
    private DataCollectionStrucure $dataCollectionStrucure;

    public function __construct()
    {
        $this->dataTableStructure = new DataTableStructure();
        $this->columnCollectionStructure = new ColumnCollectionStructure();
        $this->dataCollectionStrucure = new DataCollectionStrucure();
    }

    /**
     * @required
     */
    #[Required]
    public function setContainer(ContainerInterface $container): ?ContainerInterface
    {
        $previous = $this->container;
        $this->container = $container;

        return $previous;
    }

    public static function getSubscribedServices(): array
    {
        return [
            'parameter_bag' => '?'.ContainerBagInterface::class,
        ];
    }

    /**
     * @return DataTableStructure|null
     */
    public function getDataTableStructure()
    {
        return $this->dataTableStructure;
    }

    /**
     * @param DataTableStructure $dataTableStructure
     */
    public function setDataTableStructure(DataTableStructure $dataTableStructure): void
    {
        $this->dataTableStructure = $dataTableStructure;
    }

    /**
     * @return ColumnCollectionStructure
     */
    public function getColumnCollectionStructure(): ColumnCollectionStructure
    {
        return $this->columnCollectionStructure;
    }

    /**
     * @param ColumnCollectionStructure $columnCollectionStructure
     */
    public function setColumnCollectionStructure(ColumnCollectionStructure $columnCollectionStructure): void
    {
        $this->columnCollectionStructure = $columnCollectionStructure;
    }

    /**
     * @return DataCollectionStrucure
     */
    public function getDataCollectionStrucure(): DataCollectionStrucure
    {
        return $this->dataCollectionStrucure;
    }

    /**
     * @param DataCollectionStrucure $dataCollectionStrucure
     */
    public function setDataCollectionStrucure(DataCollectionStrucure $dataCollectionStrucure): void
    {
        $this->dataCollectionStrucure = $dataCollectionStrucure;
    }

    /**
     * @param string $name
     * @param string $selector
     * @param bool $isSortable
     * @param int $grow
     * @param bool $isButton
     * @return ColumnStructure
     */
    public function addColumn(string $name, string $selector, bool $isSortable = false, int $grow = 1, bool $isButton = false): ColumnStructure
    {
        $column = new ColumnStructure();
        $column->setName($name);
        $column->setSelector($selector);
        $column->setSortable($isSortable);
        $column->setGrow($grow);
        $column->setButton($isButton);

        $this->getColumnCollectionStructure()->addColumn($column);
        $this->getDataTableStructure()->setColumnsCollection($this->getColumnCollectionStructure());

        return $column;
    }

    /**
     * @param ItemStructure[] $data
     * @return void
     */
    public function addRow(array $data): void
    {
        $dataStructure = new DataStructure();
        $dataStructure->setItems($data);

        $this->getDataCollectionStrucure()->addData($dataStructure);
        $this->getDataTableStructure()->setDataCollection($this->getDataCollectionStrucure());
    }

    /**
     * @param string $selector
     * @param $content
     * @return ItemStructure
     */
    public function getItemStructure(string $selector, $content)
    {
        $itemStructure = new ItemStructure($selector);
        $itemStructure->setContent($content);

        return $itemStructure;
    }

    /**
     * @param string $selector
     * @param ActionContentInterface $content
     * @return ActionItemStructure
     */
    public function getActionItemStructure(string $selector, $content)
    {
        $itemStructure = new ActionItemStructure($selector);
        $itemStructure->setContent($content);

        return $itemStructure;
    }

    /**
     * @param array $items
     * @return LinkContentStructure
     */
    public function getLinkContentStructure(array $items)
    {
        $actionContent = new LinkContentStructure();
        foreach ($items as $item)
        {
            $actionContent->addItem(
                $item['href'],
                $item['title'],
                $item['type'],
                $item['target']
            );
        }

        return $actionContent;
    }

    /**
     * @param string $href
     * @param string $title
     * @param string $type
     * @param string $target
     * @return string[]
     */
    public function getLinkContentItem(string $href, string $title, string $type, string $target = '_self')
    {
        return array(
            'href' => $href,
            'title' => $title,
            'type' => $type,
            'target' => $target
        );
    }

    /**
     * Vrátí DataTable ve formátu FE
     * @return array|null
     */
    public function createView()
    {
        $dataTableStructure = $this->getDataTableStructure();
        if (!$dataTableStructure instanceof DataTableStructure)
            return null;

        $data = array(
            'columns' => $dataTableStructure->getColumnsCollectionArray(),
            'data' => $dataTableStructure->getDataCollectionArray(),
            'selectableRows' => $dataTableStructure->isSelectableRows()
        );

        return $data;
    }
}
