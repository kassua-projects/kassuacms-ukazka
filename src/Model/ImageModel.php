<?php

namespace Kassua\CMSCore\Model;

use Doctrine\ORM\EntityManagerInterface;
use Kassua\CMSCore\Entity\Gallery\ImageEntity;
use Kassua\CMSCore\Structure\FileStructure;
use Kassua\CMSCore\Structure\Gallery\GalleryStructure;
use Kassua\CMSCore\Structure\Gallery\ImageStructure;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Contracts\Service\Attribute\Required;
use Symfony\Contracts\Service\ServiceSubscriberInterface;

class ImageModel implements ServiceSubscriberInterface
{
    const PUBLIC_PATH = '/public';
    const UPLOADS_PATH = '/kassuacms-uploads';

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct()
    {
    }

    /**
     * @required
     */
    #[Required]
    public function setContainer(ContainerInterface $container): ?ContainerInterface
    {
        $previous = $this->container;
        $this->container = $container;

        return $previous;
    }

    public static function getSubscribedServices(): array
    {
        return [
            'parameter_bag' => '?' . ContainerBagInterface::class,
        ];
    }

    /**
     * @return mixed
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getProjectDir()
    {
        return $this->container->get('parameter_bag')->get('kernel.project_dir');
    }

    /**
     * @return string
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getPublicPath()
    {
        return $this->getProjectDir() . self::PUBLIC_PATH;
    }

    /**
     * @return string
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getUploadsPath()
    {
        return $this->getPublicPath() . self::UPLOADS_PATH;
    }

    /**
     * @param string $name
     * @param string $imageString
     * @return ImageStructure
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function saveImageFromString(string $name, string $imageString, string $dirInUploads = 'temp/')
    {
        $path = $this->getUploadsPath() . '/' . $dirInUploads;
        $fullPath = $path . $name;

        if (file_exists($fullPath))
        {
            return $this->saveImageFromString(date("Y-m-d+h:i:sa") . '-' . $name, $imageString, $dirInUploads);
        }

        if (!is_dir($path))
        {
            mkdir($path, 0755, true);
        }

        $myfile = fopen($fullPath, "w");
        fwrite($myfile, $imageString);
        fclose($myfile);

        $file = new File($fullPath);
        $file = $file->move($file->getPath(), hash('md5', $file->getBasename('.' . $file->getExtension())) . '.' . $file->getExtension());

        $entity = new ImageEntity();
        $entity->fromFile($file, self::UPLOADS_PATH . '/' . $dirInUploads . $file->getFilename());

        return ImageStructure::fromEntity($entity);
    }

    /**
     * @param GalleryStructure $galleryStructure
     * @param string $dirInUploads
     * @return GalleryStructure
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function moveGalleryToDirInUploads(GalleryStructure $galleryStructure, string $dirInUploads)
    {
        $images = array();
        foreach ($galleryStructure->getImages() as $imageStructure)
        {
            $images[] = $this->moveImageToDirInUploads($imageStructure, $dirInUploads);
        }

        $galleryStructure->setImages($images);

        return $galleryStructure;
    }

    /**
     * @param ImageStructure $imageStructure
     * @param string $dirInUploads
     * @return ImageStructure
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function moveImageToDirInUploads(ImageStructure $imageStructure, string $dirInUploads)
    {
        $entity = new ImageEntity();
        $entity->fromStructure($imageStructure);
        $file = $entity->getFile();

        $path = $this->getUploadsPath();
//        dump($file);
        $fullPath = $path . '/' . $dirInUploads;
        $file = $file->move($fullPath);
//        dump($file);
        $entity->fromFile($file, self::UPLOADS_PATH . '/' . $dirInUploads . $file->getFilename());

        return ImageStructure::fromEntity($entity);
    }

    /**
     * @param string $publicPath
     * @param string $name
     * @return ImageStructure
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getImageStrucureFromPublicPath(string $publicPath)
    {
        $entity = new ImageEntity();
        $entity->setPublicPath($publicPath);
        $entity->setFullPath($this->getPublicPath() . $publicPath);

        return $entity->toStructure();
    }
}
