<?php

namespace Kassua\CMSCore\Model;

use Kassua\CMSCore\Structure\Gallery\GalleryStructure;
use Kassua\CMSCore\Structure\Object\Field\TextFieldStructure;
use Kassua\CMSCore\Structure\Object\ObjectStructure;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Contracts\Service\Attribute\Required;
use Symfony\Contracts\Service\ServiceSubscriberInterface;

class ObjectModel implements ServiceSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /** @var ObjectStructure */
    private $objectStructure;

    /**
     * @required
     */
    #[Required]
    public function setContainer(ContainerInterface $container): ?ContainerInterface
    {
        $previous = $this->container;
        $this->container = $container;

        return $previous;
    }

    public static function getSubscribedServices(): array
    {
        return [
            'parameter_bag' => '?'.ContainerBagInterface::class,
        ];
    }

    /**
     * @return ObjectStructure
     */
    public function getObjectStructure(): ObjectStructure
    {
        if (!$this->objectStructure instanceof ObjectStructure)
            $this->objectStructure = new ObjectStructure();

        return $this->objectStructure;
    }

    /**
     * @param ObjectStructure $objectStructure
     */
    public function setObjectStructure(ObjectStructure $objectStructure): void
    {
        $this->objectStructure = $objectStructure;
    }

    /**
     * @param FormInterface $form
     * @return void
     */
    public function setForm(FormInterface $form): void
    {
        $object = $this->getObjectStructure()->setSymfonyForm($form);
    }

    /**
     * @return array
     */
    public function createView()
    {
        $object = $this->getObjectStructure();

        if (!$object->getSymfonyForm() instanceof FormInterface)
            return array();

        $formView = $object->getSymfonyForm()->createView();
        $data = array(
            'form' => $this->getFormForFrontendFromView($formView)
        );

        return $data;
    }

    public function getFormForFrontendFromView(FormView $formView)
    {
        $children = array();
        foreach ($formView->children as $child)
        {
            $children[] = $this->getFormForFrontendFromView($child);
        }

        $formVars = $formView->vars;
        $vars = array(
            'full_name' => $formVars['block_prefixes'][1] == 'gallery' ? substr($formVars['full_name'], 0, -2) : $formVars['full_name'],
            'name' => $formVars['name'],
            'label' => $formVars['label'],
            'block_prefixes' => $formVars['block_prefixes'],
        );
        if (isset($formVars['attr'])) $vars['attr'] = $formVars['attr'];
        if (isset($formVars['method'])) $vars['method'] = $formVars['method'];
        if (isset($formVars['action'])) $vars['action'] = $formVars['action'];
        if (isset($formVars['value'])) $vars['value'] = $formVars['value'];
        if (isset($formVars['required'])) $vars['required'] = $formVars['required'];
        if (isset($formVars['choices'])) $vars['choices'] = $formVars['choices'];
        if (isset($formVars['multiple'])) $vars['multiple'] = $formVars['multiple'];
        if (isset($formVars['prototype'])) $vars['prototype'] = $this->getFormForFrontendFromView($formVars['prototype']);

        $data = array(
            'vars' => $vars,
            'children' => $children,
        );

        return $data;
    }
}
