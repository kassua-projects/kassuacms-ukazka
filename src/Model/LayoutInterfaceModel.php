<?php

namespace Kassua\CMSCore\Model;

use Kassua\CMSCore\Structure\AdminFrontend\AdminPropsStructure;
use Kassua\CMSCore\Structure\AdminFrontend\MenuItemStructure;
use Kassua\CMSCore\Structure\AdminFrontend\MenuStructure;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class LayoutInterfaceModel
{
    /**
     * Definuje strukturu pro interface adminu
     * @var AdminPropsStructure
     */
    private AdminPropsStructure $layoutInterfaceStructure;

    /**
     * Všechny menu z configu bundlu
     * @var MenuStructure[]
     */
    private array $menus = array();

    public function __construct(private ParameterBagInterface $parameterBag)
    {
        $this->layoutInterfaceStructure = new AdminPropsStructure();

        $primaryMenu = $this->getMenuByType('primary');
        if (!empty($primaryMenu))
            $this->setPrimaryMenu($primaryMenu);
    }

    /**
     * Lze získat paramentr z configu containeru
     * @param string $name
     * @return void
     */
    private function getParameter(string $name)
    {
        return $this->parameterBag->get($name);
    }

    /**
     * Struktura pro naplnění dat, která se pak předají FE
     * @return AdminPropsStructure
     */
    private function getLayoutInterfaceStructure()
    {
        return $this->layoutInterfaceStructure;
    }

    /**
     * Nastaví titulek stránce.
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title)
    {
        $this->getLayoutInterfaceStructure()->setTitle($title);

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->getLayoutInterfaceStructure()->getTitle();
    }

    /**
     * Nastaví hlavní menu stránce.
     * @param MenuStructure|string $menu
     * @return $this
     */
    public function setPrimaryMenu(MenuStructure|string $menu)
    {
        if (is_string($menu))
            $menu = $this->getMenuByType($menu);

        $this->getLayoutInterfaceStructure()->setPrimaryMenu($menu);

        return $this;
    }

    /**
     * @return MenuStructure|null
     */
    public function getPrimaryMenu()
    {
        return $this->getLayoutInterfaceStructure()->getPrimaryMenu();
    }

    /**
     * Nastaví sekundární menu na stránce
     * @param MenuStructure|string $menu
     * @return $this
     */
    public function setSecondaryMenu(MenuStructure|string $menu)
    {
        if (is_string($menu))
            $menu = $this->getMenuByType($menu);

        $this->getLayoutInterfaceStructure()->setSecondaryMenu($menu);

        return $this;
    }

    /**
     * @return MenuStructure|null
     */
    public function getSecondaryMenu()
    {
        return $this->getLayoutInterfaceStructure()->getSecondaryMenu();
    }

    /**
     * Najde menu podle typu z konfigu
     * @param string $type
     * @return MenuStructure|null
     */
    public function getMenuByType(string $type)
    {
        $menus = $this->getMenusFromConfig();
        if (!empty($menus[$type]))
            return $menus[$type];

        return null;
    }

    /**
     * Vrátí a pokud ještě nejsou načetlé menu z configu, tak je načte
     * @return MenuStructure[]
     */
    public function getMenusFromConfig()
    {
        if (empty($this->menus))
        {
            $menus = $this->getParameter('kassua_cms_core.menus');
            if (is_array($menus))
            {
                foreach ($menus as $menu)
                {
                    $menuStructure = $this->getMenuStructureFromArray($menu);
                    $this->menus[$menuStructure->getType()] = $menuStructure;
                }
            }
        }

        return $this->menus;
    }

    /**
     * Převede pole do struktury a vrátí ji.
     * @param array $menu
     * @return MenuStructure
     */
    public function getMenuStructureFromArray(array $menu)
    {
        $menuStructure = new MenuStructure();
        $menuStructure->setType($menu['type']);
        if (!empty($menu['title'])) $menuStructure->setTitle($menu['title']);

        foreach ($menu['items'] as $item) {
            $menuStructure->addItem(MenuItemStructure::fromArray($item));
        }

        return $menuStructure;
    }

    /**
     * Nastaví aktivní route
     * @param string $route
     * @return $this
     */
    public function setActiveRoute(string $route)
    {
        $this->getLayoutInterfaceStructure()->setActiveRoute($route);

        return $this;
    }

    /**
     * @return string
     */
    public function getActiveRoute()
    {
        return $this->getLayoutInterfaceStructure()->getActiveRoute();
    }

    /**
     * Nastaví username
     * @param string $username
     * @return $this
     */
    public function setUserName(string $username)
    {
        $this->getLayoutInterfaceStructure()->setUserName($username);

        return $this;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->getLayoutInterfaceStructure()->getUserName();
    }

    /**
     * @param string $link
     * @return $this
     */
    public function setLogoutLink(string $link)
    {
        $this->getLayoutInterfaceStructure()->setLogoutLink($link);

        return $this;
    }

    /**
     * @return string
     */
    public function getLogoutLink()
    {
        return $this->getLayoutInterfaceStructure()->getLogoutLink();
    }

    /**
     * @param string $link
     * @return $this
     */
    public function setWebLink(string $link)
    {
        $this->getLayoutInterfaceStructure()->setWebLink($link);

        return $this;
    }

    /**
     * @return string
     */
    public function getWebLink()
    {
        return $this->getLayoutInterfaceStructure()->getWebLink();
    }

    /**
     * @param string $link
     * @return $this
     */
    public function setProfileLink(string $link)
    {
        $this->getLayoutInterfaceStructure()->setProfileLink($link);

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileLink()
    {
        return $this->getLayoutInterfaceStructure()->getProfileLink();
    }
}
