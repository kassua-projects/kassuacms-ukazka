<?php

namespace Kassua\CMSCore\Service;

use Kassua\CMSCore\Entity\TypeEntity;
use Kassua\CMSCore\Model\DataTableModel;
use Kassua\CMSCore\Model\LayoutInterfaceModel;
use Kassua\CMSCore\Model\ObjectModel;
use Kassua\CMSCore\Model\RoleForRouteModel;
use Kassua\CMSCore\Structure\AdminFrontend\MenuItemStructure;
use Kassua\CMSCore\Structure\AdminFrontend\MenuStructure;
use Kassua\CMSCore\Structure\DataTable\DataTableStructure;
use Kassua\CMSCore\Structure\Layout\LayoutStructure;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class LayoutService
{
    protected LayoutStructure $layoutStructure;

    public function __construct(protected LayoutInterfaceModel  $layoutInterface,
                                protected ParameterBagInterface $parameterBag,
                                protected UrlGeneratorInterface $urlGenerator,
                                protected Security              $security,
                                protected RoleForRouteModel     $roleForRoute)
    {
        $this->layoutStructure = new LayoutStructure();
    }

    /**
     * @return LayoutStructure
     */
    private
    function getLayoutStructure()
    {
        return $this->layoutStructure;
    }

    /**
     * @return LayoutInterfaceModel
     */
    public
    function getInterface()
    {
        return $this->layoutInterface;
    }

    /**
     * Vrátí data pro frontend
     * @return array
     */
    public
    function createView()
    {
        $interface = $this->getInterface();

        $contentProps = array();
        if ($this->getDataTable() instanceof DataTableModel)
            $contentProps['dataTable'] = $this->getDataTable()->createView();
        elseif ($this->getContentObject() instanceof ObjectModel)
            $contentProps['object'] = $this->getContentObject()->createView();

        $contentProps = array_merge($contentProps, $this->getLayoutStructure()->getContent()->getContentProps());
        $params = array(
            'backendProps' => $contentProps,
            'adminProps' => array(
                'title' => $interface->getTitle(),
                'primaryMenu' => $interface->getPrimaryMenu() !== null ? $this->createMenuViewFromStructure($interface->getPrimaryMenu()) : null,
                'secondaryMenu' => $interface->getSecondaryMenu() !== null ? $this->createMenuViewFromStructure($interface->getSecondaryMenu()) : null,
                'userName' => $interface->getUserName(),
                'logoutLink' => $interface->getLogoutLink(),
                'webLink' => $interface->getWebLink(),
                'profileLink' => $interface->getProfileLink()
            )
        );

        return $params;
    }

    /**
     * @param MenuStructure $menu
     * @return array
     */
    private
    function createMenuViewFromStructure(MenuStructure $menu)
    {
        $items = array();
        foreach ($menu->getItems() as $item) {
            $roleForRoute = $this->roleForRoute->getRoleForRoute($item->getRoute());
            if ($roleForRoute !== null && !$this->security->isGranted($roleForRoute))
                continue;

            $items[] = array(
                'url' => $this->urlGenerator->generate($item->getRoute()),
                'title' => $item->getTitle(),
                'is_active' => $this->isRouteActive($item),
                'icon' => $item->getIcon()
            );
        }

        $return = array(
            'type' => $menu->getType(),
            'title' => $menu->getTitle(),
            'items' => $items
        );

        return $return;
    }

    /**
     * Zjistí jestli se uživatel aktuálně nachází v dané route
     * @param MenuItemStructure $itemStructure
     * @return bool
     */
    public
    function isRouteActive(MenuItemStructure $itemStructure)
    {
        if ($itemStructure->getRoute() == $this->getInterface()->getActiveRoute() ||
            ($this->getInterface()->getSecondaryMenu() instanceof MenuStructure &&
                $itemStructure->getMenuType() == $this->getInterface()->getSecondaryMenu()->getType()))
            return true;

        $explRoute = explode('.', $itemStructure->getRoute());
        $explActualRoute = explode('.', $this->getInterface()->getActiveRoute());

        if (count($explRoute) > 1 && count($explActualRoute) > 1 &&
            ($explRoute[count($explRoute) - 2] === $explActualRoute[count($explActualRoute) - 2]))
            return true;

        return false;
    }

    /**
     * Nastaví datatable do layoutu
     * @param DataTableStructure $dataTableStructure
     * @return $this
     */
    public function setDataTable(DataTableModel $dataTable)
    {
        $this->getLayoutStructure()->getContent()->setDataTable($dataTable);

        return $this;
    }

    /**
     * @return DataTableModel|null
     */
    public function getDataTable()
    {
        return $this->getLayoutStructure()->getContent()->getDataTable();
    }

    /**
     * Nastaví objekt (form) do layoutu
     * @param ObjectModel $objectModel
     * @return $this
     */
    public function setContentObject(ObjectModel $objectModel)
    {
        $this->getLayoutStructure()->getContent()->setObject($objectModel);

        return $this;
    }

    /**
     * @return ObjectModel|null
     */
    public function getContentObject()
    {
        return $this->getLayoutStructure()->getContent()->getObject();
    }

    /**
     * Přidá custom property do content sekce
     * @param $key
     * @param $value
     * @return $this
     */
    public function addContentProp($key, $value)
    {
        $this->getLayoutStructure()->getContent()->addContentProp($key,$value);

        return $this;
    }

    /**
     * Přidá button pro přidání záznamu do content sekce
     * @param string $url
     * @param string $title
     * @return $this
     */
    public function addAddButton(string $url, string $title = 'Přidat záznam')
    {
        return $this->addContentProp(
            'actionLink',
            $this->getActionLinkStructure(
                $url,
                $title,
                TypeEntity::ADD
            )
        );
    }

    /**
     * @param string $href
     * @param string $title
     * @param string $type
     * @return string[]
     */
    public function getActionLinkStructure(string $href, string $title, string $type)
    {
        return array(
            'link' => $href,
            'title' => $title,
            'type' => $type
        );
    }
}
