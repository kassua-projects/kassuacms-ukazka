<?php

namespace Kassua\CMSCore\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Kassua\CMSCore\Entity\TypeEntity;
use Kassua\CMSCore\Model\DataTableModel;
use Kassua\CMSCore\Model\ImageModel;
use Kassua\CMSCore\Model\ObjectModel;
use Kassua\CMSCore\Model\RoleForRouteModel;
use Kassua\CMSCore\Structure\AdminFrontend\AdminFrontendStructure;
use Kassua\CMSCore\Structure\AdminFrontend\MenuStructure;
use Kassua\CMSCore\Structure\Gallery\GalleryStructure;
use Kassua\CMSCore\Structure\User\UserStructure;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Service\Attribute\Required;
use Symfony\Contracts\Service\ServiceSubscriberInterface;

class AdminService implements ServiceSubscriberInterface
{
    private $backendProps = array();

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(protected ImageModel                  $imageModel,
                                protected UserRepository              $userRepository,
                                protected UserPasswordHasherInterface $passwordHasher,
                                protected Security                    $security,
                                protected RoleForRouteModel           $roleForRouteModel)
    {
    }

    /**
     * @required
     */
    #[Required]
    public function setContainer(ContainerInterface $container): ?ContainerInterface
    {
        $previous = $this->container;
        $this->container = $container;

        return $previous;
    }

    public static function getSubscribedServices(): array
    {
        return [
            'router' => '?' . RouterInterface::class,
            'security.token_storage' => '?' . TokenStorageInterface::class,
            'security.role_hierarchy.roles' => '?' . RoleHierarchyInterface::class
        ];
    }

    /**
     * @return RouterInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function getRouter(): RouterInterface
    {
        return $this->container->get('router');
    }

    /**
     * @return ImageModel
     */
    public function getImageModel()
    {
        return $this->imageModel;
    }

    /**
     * @return UserRepository
     */
    public function getUserRepository()
    {
        return $this->userRepository;
    }

    /**
     * @return UserPasswordHasherInterface
     */
    public function getPasswordHasher()
    {
        return $this->passwordHasher;
    }

    /**
     * @return Security
     */
    public function getSecurity()
    {
        return $this->security;
    }

    /**
     * @return RoleForRouteModel
     */
    public function getRoleForRouteModel()
    {
        return $this->roleForRouteModel;
    }

    /**
     * @return UserInterface
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getUser()
    {
        if (!$this->container->has('security.token_storage')) {
            throw new \LogicException('The SecurityBundle is not registered in your application. Try running "composer require symfony/security-bundle".');
        }

        if (null === $token = $this->container->get('security.token_storage')->getToken()) {
            return null;
        }

        return $token->getUser();
    }

    /**
     * @return string|null
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getUserIdentifier()
    {
        $user = $this->getUser();
        if (!$user instanceof UserInterface)
            return null;

        return $user->getUserIdentifier();
    }

    /**
     * @return string[]
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getRoleHierarchy()
    {
        if (!$this->container->has('security.role_hierarchy.roles')) {
            throw new \LogicException('Security roles not in app');
        }

        /** @var RoleHierarchyInterface $roles */
        $roles = $this->container->get('security.role_hierarchy.roles');

        return $roles->getReachableRoleNames($this->getUser()->getRoles());
    }

    /**
     * Vrátí role pro collectionType ve formuláři
     * @return array
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getRoleHierarchyForForm()
    {
        $array = array();
        foreach ($this->getRoleHierarchy() as $role) {
            $array[$role] = $role;
        }

        return $array;
    }

    /**
     * Vrátí true, pokud email je již registrován
     * @param string $email
     * @return bool
     */
    public function isEmailRegistered(string $email)
    {
        $user = $this->getUserRepository()->findBy(array('email' => $email));

        return !empty($user);
    }

    /**
     * Ověří, zda je možné uživatele registrovat
     * @param UserStructure $entity
     * @return bool
     */
    public function isPossibleToRegisterUser(UserStructure $entity)
    {
        if ($entity->getEmail() === null) return false;
        if ($entity->getPassword() === null) return false;
        if ($this->isEmailRegistered($entity->getEmail())) return false;

        return true;
    }

    /**
     * Validuje, jestli je možné uložit usera z formuláře
     * @param UserStructure $entity
     * @return bool
     */
    public function isPossibleToSaveUser(UserStructure $entity)
    {
        if ($entity->getEmail() === null) return false;

        return true;
    }

    /**
     * Uloží usera, pokud je validní
     * @param UserStructure $entity
     * @return false|UserStructure
     */
    public function saveUser(UserStructure $entity)
    {
        if ($entity->getId() !== null)
            $user = $this->getUserRepository()->find($entity->getId());
        else
            $user = new User();

        if (!$this->isPossibleToSaveUser($entity, $user))
            return false;

        $user->setEmail($entity->getEmail());
        $user->setRoles($entity->getRoles());
        if ($entity->getPassword() !== null) $user->setPassword($this->getPasswordHasher()->hashPassword($user, $entity->getPassword()));

        $this->getUserRepository()->save($user, true);
        $entity->setId($user->getId());

        return $entity;
    }

    /**
     * Registruje uživatele, pokud je validní
     * @param UserStructure $entity
     * @return false|UserStructure
     */
    public function registerUser(UserStructure $entity)
    {
        if (!$this->isPossibleToRegisterUser($entity)) return false;

        return $this->saveUser($entity);
    }
}
