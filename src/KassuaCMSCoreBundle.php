<?php

namespace Kassua\CMSCore;

class KassuaCMSCoreBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }
}
