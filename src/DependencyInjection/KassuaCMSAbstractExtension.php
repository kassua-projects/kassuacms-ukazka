<?php

namespace Kassua\CMSCore\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;

class KassuaCMSAbstractExtension extends \Symfony\Component\DependencyInjection\Extension\Extension implements \Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface
{

    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container)
    {
    }

    /**
     * @inheritDoc
     */
    public function prepend(ContainerBuilder $container)
    {
    }


    /**
     * Mergne exitující hierarchii rolí do sebe.
     * Poté předá role containeru a propíše do nastavení
     * @param ContainerBuilder $container
     * @param array $roles
     * @return void
     */
    protected function setRoleHierarchyToContainer(ContainerBuilder $container, array $roles)
    {
        $container->setParameter('security.role_hierarchy.roles', array_merge_recursive($roles, $container->getParameter('security.role_hierarchy.roles')));
    }
}
