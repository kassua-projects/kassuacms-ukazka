<?php

namespace Kassua\CMSCore\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;

class KassuaCMSCoreExtension extends KassuaCMSAbstractExtension
{
    private $roles = array(
        'ROLE_SUPERADMIN' => ['ROLE_ADMIN'],
        'ROLE_ADMIN' => ['ROLE_ADMIN_ACCESS', 'ROLE_USERS_ADMIN'],
        'ROLE_USERS_ADMIN' => ['ROLE_USER_COLLECTION', 'ROLE_USER_ADD', 'ROLE_USER_EDIT']
    );

    private $roleForRoute = array(
        [
            'route' => 'kassua_cms.cms.users.collection',
            'role' => 'ROLE_USER_COLLECTION'
        ],
        [
            'route' => 'kassua_cms.cms.users.add',
            'role' => 'ROLE_USER_ADD'
        ],
        [
            'route' => 'kassua_cms.cms.users.edit',
            'role' => 'ROLE_USER_EDIT'
        ]
    );

    public function prepend(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('security', [
            'providers' => [
                'kassua_user_provider' => [
                    'entity' => [
                        'class' => 'App\Entity\User',
                        'property' => 'email'
                    ]
                ]
            ],
            'role_hierarchy' => $this->roles,
            'firewalls' => [
                'main' => [
                    'lazy' => true,
                    'provider' => 'kassua_user_provider',
                    'form_login' => [
                        'login_path' => 'kassua_cms.login',
                        'check_path' => 'kassua_cms.login'
                    ],
                    'logout' => [
                        'path' => 'kassua_cms.logout'
                    ]
                ]
            ],
            'access_control' => [
                ['path' => '^/admin/(en|cs)/login', 'roles' => ['PUBLIC_ACCESS']],
                ['path' => '^/admin/public', 'roles' => ['PUBLIC_ACCESS']],
                ['path' => '^/admin', 'roles' => ['ROLE_ADMIN_ACCESS']],
            ]
        ]);
    }

    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('kassua_cms_core.menus', $config['menus']);
        $container->setParameter('kassua_cms_core.roleForRoute', array_merge($config['roleForRoute'], $this->roleForRoute));

        $loader = new PhpFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../../config')
        );
        $loader->load('services.php');
    }
}
