<?php

namespace Kassua\CMSCore\DependencyInjection;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('kassua_cms_core');

        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('menus')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('type')->end()
                            ->scalarNode('title')->end()
                            ->arrayNode('items')
                                ->arrayPrototype()
                                    ->children()
                                        ->scalarNode('title')->end()
                                        ->scalarNode('route')->end()
                                        ->scalarNode('icon')->end()
                                        ->scalarNode('menuType')->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('roleForRoute')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('route')->end()
                            ->scalarNode('role')->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
