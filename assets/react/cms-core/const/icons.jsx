import React from "react";

import InstagramIcon from "@mui/icons-material/Instagram";
import {DoDisturbAlt} from "@mui/icons-material";
import EditIcon from '@mui/icons-material/Edit';

export default function getIcon(name) {
    switch (name) {
        case 'instagram':
            return <InstagramIcon></InstagramIcon>;
            break;
        case 'edit':
            return <EditIcon></EditIcon>
            break;
        default:
            return <DoDisturbAlt></DoDisturbAlt>
            break;
    }
}
