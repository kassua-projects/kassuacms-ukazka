import React from "react";
import {InputLabelKassua, TextFieldKassua} from "../../styles/formStyles";
import {FieldContainer} from "./styles";

function NumberTypeFunction({field}) {
    return (
        <FieldContainer>
            {field.vars.label && (
                <InputLabelKassua labelfor={field.vars.name}>
                    {field.vars.label}
                </InputLabelKassua>
            )}
            <TextFieldKassua type={"number"} />
        </FieldContainer>
    );
}

class NumberType extends React.Component {
    constructor() {
        super();
    }

    render() {
        const field = this.props.field;

        return <NumberTypeFunction field={field} min={0}></NumberTypeFunction>
    }
}

export default NumberType;
