import React from "react";
import {InputLabelKassua, TextFieldKassua} from "../../styles/formStyles";
import {FieldContainer, LabelWidthItemContainer} from "./styles";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";

function SelectTypeFunction({field, inputRef})
{
    console.log(field);
    return (
        <FieldContainer>
            {field.label && (
                <InputLabelKassua labelfor={field.name}>
                    {field.label}
                </InputLabelKassua>
            )}
            <LabelWidthItemContainer>
                <Select
                    name={field.full_name}
                    defaultValue={field.value}
                    label=""
                    required={field.required}
                >
                    {field.choices.map((choice, index) => {
                        return(
                            <MenuItem key={index} value={choice.value}>{choice.label}</MenuItem>
                        );
                    })}
                </Select>
            </LabelWidthItemContainer>
        </FieldContainer>
    );
}

class SelectType extends React.Component {
    constructor() {
        super();
    }

    render() {
        const field = this.props.field;
        const inputRef = this.props.inputRef;

        return <SelectTypeFunction field={field} inputRef={inputRef}></SelectTypeFunction>
    }
}

export default SelectType;
