import React, {useEffect, useState, useRef} from "react";
import TextType from "../fields/TextType";
import ImageType from "../fields/ImageType";
import {
    ActualField,
    ActualFieldContainer,
    CollectionContainer, NewField,
    TextItemContainer,
    TextItemContainerLast
} from "./styles";
import Icon from "../icon/Icon";
import {mobileMax} from "../../const/breakpoints";
import {InputLabelKassua, TextFieldKassua} from "../../styles/formStyles";
import ObjectFields from "../object/ObjectFields";

function CollectionTypeFunction({collectionField}) {
    const inputRef = useRef();
    const [tfValue, setTFValue] = useState('');
    const fieldsRef = useRef([]);

    const [autoIncrementFields, setAutoIncrementFields] = useState(() => {
        return collectionField.children.length > 0 ?
            parseInt(collectionField.children[collectionField.children.length - 1].vars.name) : -1;
    });
    console.log(collectionField);
    const [fields, setFields] = useState(collectionField.children);
    const prototype = collectionField.vars.prototype.children.length === 0 ? [collectionField.vars.prototype] : collectionField.vars.prototype.children;
    const [prototypeField, setPrototypeField] = useState(createNewField(autoIncrementFields + 1));

    // function getFieldFromPrototype() {
    //     const newField = prototype;
    //     return newField;
    // }

    // useEffect(() => {
    //     setPrototypeField(prototype);
    // }, fields);

    function createNewField(i)
    {
        const newField = processNewField(collectionField.vars.prototype, i);
        newField.vars.name = i;
        // console.log(newField);
        return newField;
        // console.log('field');
        // console.log(collectionField.vars.prototype.vars);
        // console.log('prototype');
        // console.log(prototype);
        const copyProto = JSON.parse(JSON.stringify(collectionField.vars.prototype))
        const newFieldProps = copyProto.vars;
        newFieldProps.full_name = newFieldProps.full_name.replace(newFieldProps.name, i);
        // newFieldProps.label = null;
        // const newField = {vars: newFieldProps, children: []};
        //
        copyProto.children.forEach((ele) => {
            // console.log(ele);
            const vars = JSON.parse(JSON.stringify(ele.vars));
            vars.full_name = vars.full_name.replace(newFieldProps.name, i);
            // vars.name = i;
            // if (vars.prototype !== undefined)
            //     vars.prototype.full_name

            newField.children.push({vars: vars, children: []});
        });
        newFieldProps.name = i;
        // console.log(newField);

        return newField;
    }

    function processNewField(proto, protoName, protoNameToReplace = collectionField.vars.prototype.vars.name)
    {
        const copyProto = JSON.parse(JSON.stringify(proto));
        const newFieldProps = copyProto.vars;

        newFieldProps.full_name = newFieldProps.full_name.replace(protoNameToReplace, protoName);
        if (newFieldProps.prototype !== undefined)
            newFieldProps.prototype = processNewField(newFieldProps.prototype, protoName, protoNameToReplace);
        // newFieldProps.name = protoName;
        // newFieldProps.label = null;
        const newField = {vars: newFieldProps, children: []};
        //
        copyProto.children.forEach((ele) => {
            // console.log(ele);
            // const vars = JSON.parse(JSON.stringify(ele.vars));
            // vars.full_name = vars.full_name.replace(newFieldProps.name, i);
            // vars.name = i;
            // if (vars.prototype !== undefined)
            //     vars.prototype.full_name

            newField.children.push(processNewField(ele, protoName, protoNameToReplace));
        });
        // console.log(newField);

        return newField;
    }

    function processProtoChildren(ele)
    {
        const element = document.getElementsByName(ele.vars.full_name)[0];
        // console.log(element);
        // console.log(ele);
        const vars = JSON.parse(JSON.stringify(ele.vars));
        // console.log(element);
        if (element !== undefined)
        {
            vars.value = element.value;

        }

        const newProtoChild = {vars: vars, children: []};

        if (ele.children.length > 0)
        {
            ele.children.forEach((child) => {
                newProtoChild.children.push(processProtoChildren(child));
            });
        }
        else if (ele.vars.block_prefixes[1] === 'collection')
        {
            const collElm = document.querySelectorAll('input[name^="' + ele.vars.full_name + '["]');
            let lastI = null;
            let lastSaved = null;
            let newCollProtoField = null;
            collElm.forEach((collChild) => {
                const explCollChild = collChild.getAttribute("name").split('[');
                if (lastI !== explCollChild[explCollChild.length - 2].slice(0, -1))
                {
                    if (lastI !== null)
                    {
                        lastSaved = lastI;
                        newProtoChild.children.push(processProtoChildren(newCollProtoField));
                    }
                    lastI = explCollChild[explCollChild.length - 2].slice(0, -1);
                    newCollProtoField = processNewField(ele.vars.prototype, lastI, ele.vars.prototype.vars.name);
                    newCollProtoField.vars.name = lastI;
                    // newCollProtoField.children = [];
                    console.log(lastI);
                    // console.log(ele.vars.prototype.name);
                    console.log(ele);
                }

                newCollProtoField.children.forEach((collProtoChild) => {
                    const explCollProtoChild = collProtoChild.vars.full_name.split('[');
                    if (explCollChild[explCollChild.length - 1] === explCollProtoChild[explCollProtoChild.length - 1])
                    {
                        const newCollProtoChild = processNewField(collProtoChild, explCollChild[explCollChild.length - 2].slice(0, -1), collProtoChild.vars.name);
                        // console.log(newCollProtoChild);
                        collProtoChild = processProtoChildren(newCollProtoChild);
                        // newCollProtoField.children.push(processProtoChildren(newCollProtoChild));
                    }
                });
            });
            if (lastI !== lastSaved)
            {
                newProtoChild.children.push(processProtoChildren(newCollProtoField));
                console.log(newProtoChild);
            }
            // console.log(ele);
        }
        console.log(newProtoChild);
        return newProtoChild;
    }

    function addFieldFromRef() {
        const prototypeFieldVars = JSON.parse(JSON.stringify(prototypeField.vars));
        const protoElm = document.getElementsByName(prototypeFieldVars.full_name);
        // console.log(protoElm.length);
        if (protoElm.length > 0)
        {
            // console.log(protoElm[0].value);
            prototypeFieldVars.value = protoElm[0].value;
        }
        const newField = {vars: prototypeFieldVars, children: []};

        prototypeField.children.forEach((ele) => {
            newField.children.push(processProtoChildren(ele));
            // element.value = '';
        });
        // console.log(newField);
        setFields([...fields, newField]);
        setPrototypeField(createNewField(autoIncrementFields + 2));
        setAutoIncrementFields(autoIncrementFields + 1);
    }

    function removeField(index) {
        setFields([...fields.filter((currentValue, i) => {
            return i !== index;
        })]);
    }

    return (
        <CollectionContainer>
            {collectionField.vars.label && (
                <InputLabelKassua>
                    {collectionField.vars.label}
                </InputLabelKassua>
            )}
            {fields.map((field, index) => {
                // console.log(field);
                return (
                    <ActualFieldContainer key={field.vars.full_name}>
                        <ActualField className={collectionField.vars?.attr?.class ? collectionField.vars.attr.class : ''}>
                            <ObjectFields fields={[field]}></ObjectFields>
                        </ActualField>
                        <button onClick={(e) => {
                            e.preventDefault();
                            removeField(index);
                        }}>
                            <Icon type={'remove'}></Icon>
                        </button>
                    </ActualFieldContainer>
                );
            })}
            <ActualFieldContainer>
                <NewField className={collectionField.vars?.attr?.class ? collectionField.vars.attr.class : ''}>
                    <ObjectFields fields={[prototypeField]}></ObjectFields>
                </NewField>
                <button onClick={(e) => {
                    e.preventDefault();
                    addFieldFromRef()
                }}>
                    <Icon type={'add'}></Icon>
                </button>
            </ActualFieldContainer>
        </CollectionContainer>
    );
}

// function CollectionTypeFunction({collectionField}) {
//     const inputRef = useRef();
//     const [tfValue, setTFValue] = useState('');
//     const fieldsRef = useRef([]);
//     const prototype = collectionField.prototype;
//     const [autoIncrementFields, setAutoIncrementFields] = useState(0);
//
//     const [fields, setFields] = useState(() => {
//         console.log(collectionField.value);
//         const defaultFields = typeof collectionField.value === 'object' && collectionField.value !== null ? Object.values(collectionField.value) : [];
//         let ai = 0;
//         let array = [];
//         defaultFields.forEach((elm) => {
//             array.push(getFieldFromValue(elm, ai));
//             ai++;
//         });
//         setAutoIncrementFields(ai);
//         return array;
//     });
//
//     function getFieldFromValue(value, name)
//     {
//         return {
//             'required': prototype.required,
//             'name': collectionField.name + '[' + (name).toString() + ']',
//             'type': prototype.type,
//             'value': value,
//         };
//     }
//
//     function addFieldFromRef() {
//         setFields([...fields, getFieldFromValue(inputRef?.current?.value ? inputRef.current.value : '', autoIncrementFields)]);
//         setAutoIncrementFields(autoIncrementFields + 1);
//         setTFValue('');
//     };
//
//     function removeField(index) {
//         setFields([...fields.filter((currentValue, i) => {
//             return i !== index;
//         })]);
//     }
//
//     return (
//         <>
//             {prototype.type == 'text' && (
//                 <CollectionContainer>
//                     {collectionField.label && (
//                         <InputLabelKassua>
//                             {collectionField.label}
//                         </InputLabelKassua>
//                     )}
//                     {fields.map((field, index) => {
//                         switch (field.type) {
//                             case 'text':
//                                 return (
//                                     <TextItemContainer key={field.name}>
//                                         <TextType field={field}/>
//                                         <button onClick={(e) => {
//                                             e.preventDefault();
//                                             removeField(index);
//                                         }}>
//                                             <Icon type={'remove'}></Icon>
//                                         </button>
//                                     </TextItemContainer>
//                                 );
//                                 break;
//                         }
//                     })}
//
//                     <TextItemContainerLast>
//                         <TextFieldKassua inputRef={inputRef} name={collectionField.name + '[input]'} value={tfValue}
//                                          required={false} onChange={(e) => {
//                             setTFValue(e.target.value);
//                         }} variant="outlined"/>
//                         <button onClick={(e) => {
//                             e.preventDefault();
//                             addFieldFromRef()
//                         }}>
//                             <Icon type={'add'}></Icon>
//                         </button>
//                     </TextItemContainerLast>
//                 </CollectionContainer>
//             )}
//             {prototype.type == 'file' && (
//                 <ImageType field={collectionField}></ImageType>
//             )}
//         </>
//     );
// }

class CollectionType extends React.Component {
    constructor() {
        super();
    }

    render() {
        const collectionField = this.props.collectionField;

        return (<CollectionTypeFunction collectionField={collectionField}></CollectionTypeFunction>);
    }
}

export default CollectionType;
