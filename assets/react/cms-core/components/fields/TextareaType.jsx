import React, {useState} from "react";
import {InputLabelKassua, ObjectForm, TextFieldKassua} from "../../styles/formStyles";
import ReactQuill from "react-quill";
import 'react-quill/dist/quill.snow.css';
import {FieldContainer, TextAreaContainer} from "./styles";

function TextareaTypeFunction({field, inputRef})
{
    const [tfValue, setTFValue] = useState(field.value ? field.value : '');

    return (
        <FieldContainer>
            {field.label && (
                <InputLabelKassua labelfor={field.name}>
                    {field.label}
                </InputLabelKassua>
            )}
            <TextAreaContainer>
                <ReactQuill theme="snow" value={tfValue} onChange={setTFValue}></ReactQuill>
            </TextAreaContainer>
            <input type={'hidden'} name={field.full_name} value={tfValue}/>
        </FieldContainer>
    );
}

class TextareaType extends React.Component {
    constructor() {
        super();
    }

    render() {
        const field = this.props.field;
        const inputRef = this.props.inputRef;

        return <TextareaTypeFunction field={field} inputRef={inputRef}></TextareaTypeFunction>
    }
}

export default TextareaType;
