import React, {useState} from "react";
import {InputLabelKassua, ObjectForm, TextFieldKassua} from "../../styles/formStyles";
import TextareaType from "./TextareaType";
import {FieldContainer} from "./styles";
import {InputAdornment} from "@mui/material";

function TextTypeFunction({field, inputRef})
{
    const [tfValue, setTFValue] = useState(field.value ? field.value : '');

    let type = field.block_prefixes[1];
    switch (field.block_prefixes[2])
    {
        case 'email':
        case 'password':
            type = field.block_prefixes[2];
    }

    return (
        <FieldContainer className={field.attr?.class ? field.attr.class : ''}>
            {field.label && (
                <InputLabelKassua labelfor={field.name}>
                    {field.label}
                </InputLabelKassua>
            )}
            <TextFieldKassua
                type={type}
                min={0}
                inputRef={inputRef}
                name={field.full_name}
                value={tfValue}
                required={field.required}
                onChange={(e) => {
                    setTFValue(e.target.value);
                }}
                variant={"outlined"}
                placeholder={field.attr?.placeholder ? field.attr.placeholder : ''}
                InputProps={{
                    // endAdornment: <InputAdornment position="end">kg</InputAdornment>,
                }}
            />
        </FieldContainer>
    );
}

class TextType extends React.Component {
    constructor() {
        super();
    }

    render() {
        const field = this.props.field;
        const inputRef = this.props.inputRef;

        if (field.block_prefixes[2] == 'textarea')
            return (
                <TextareaType field={field}></TextareaType>
            );

        return <TextTypeFunction field={field} inputRef={inputRef}></TextTypeFunction>
    }
}

export default TextType;
