import styled from "styled-components";
import {defaultElementBottomMargin, labelWidth, defaultElementsGap} from "../../styles/formStyles";
import {TextFieldKassua} from "../../styles/formStyles";
import {
	lightGreyColor,
	defaultBorderRadius,
	redColor,
	darkGreyColor,
	greyColor,
	secondaryColor, defaultBoxShadow, primaryColor
} from "../../controllers/styles";
import ReactQuill from "react-quill";

export const CollectionContainer = styled.div`
	display: flex;
	width: 60%;
	flex-grow: 1;
	flex-wrap: wrap;
	flex-direction: row;
	background-color: ${lightGreyColor};
	border-radius: ${defaultBorderRadius};
	row-gap: calc(${defaultElementsGap} * 3);
	padding-bottom: ${defaultElementsGap};
`;

export const TextItemContainer = styled.div`
	display: flex;
	width: 60%;
	flex-grow: 1;

	& .MuiInputBase-root {
		margin-bottom: 0;
	}

	& > ${TextFieldKassua} {
		width: 100%;
	}
`;

export const ActualFieldContainer = styled.div`
	display: flex;
	width: 100%;
	//width: 60%;
	//flex-grow: 1;
	//flex-wrap: wrap;
	//flex-direction: column;
		// padding: ${defaultElementsGap};
	border-radius: ${defaultBorderRadius};
		// border: 1px solid ${greyColor};
	gap: ${defaultElementsGap};
	padding: 0 ${defaultElementsGap};
	//margin: 8px;
`;

export const ActualField = styled.div`
	display: flex;
	width: 60%;
	flex-grow: 1;
	flex-direction: row;
	flex-wrap: wrap;
	padding: ${defaultElementsGap};
	border-radius: ${defaultBorderRadius};
	border: 2px solid ${darkGreyColor};
	row-gap: calc(${defaultElementsGap} * 2);
	column-gap: min(${defaultElementsGap}, 5%);
	// column-gap: clamp(5%, ${defaultElementsGap}, ${defaultElementsGap});
	background-color: #FFFFFF;
	//box-shadow: ${defaultBoxShadow};
	//margin: 8px;
`;

export const NewField = styled(ActualField)`
	// border: 3px solid ${secondaryColor};
`;

export const TextItemContainerLast = styled(TextItemContainer)`
	margin-bottom: ${defaultElementBottomMargin};
`;

export const LabelWidthItemContainer = styled.div`
	// width: ${labelWidth};
	width: 100%;
	display: flex;

	& .MuiInputBase-root {
		width: 100%;
	}
`;

export const GalleryButtons = styled.div`
	display: flex;
	aspect-ratio: 1/1;
	height: 200px;
	flex-direction: column;
	margin-right: 12px;
`;

export const UploadButton = styled.button`
	height: 100%;
	background-color: ${lightGreyColor};
	border-radius: ${defaultBorderRadius};
	margin-bottom: 5px;
`;

export const RemoveButton = styled.button`
	border-radius: ${defaultBorderRadius};
	background-color: ${redColor};
`;

export const GalleryImagesContainer = styled.div`
	height: 200px;
	width: 100%;
	display: flex;
	overflow-x: scroll;
`;

export const GalleryContainer = styled.div`
	display: flex;
	//margin-bottom: ${defaultElementBottomMargin};
`;

export const GalleryImagesInnerContainer = styled.div`
	display: flex;
`;

export const GalleryImageContainer = styled.div`
	display: flex;
	flex-direction: column;
	width: 200px;
	margin-right: 8px;

	& img {
		object-fit: cover;
		height: 155px;
		width: 155px;
	}
`;

export const GalleryImageInnerContainer = styled.div`
	display: flex;
	width: 100%;
	height: 100%;
	justify-content: center;
	align-items: center;
`;

export const GalleryImageButtonContainer = styled.div`
	display: flex;
	width: 100%;
`;

export const UpdateGalleryImageButton = styled.button`
	width: 50%;
	margin-right: 5px;
	background-color: ${lightGreyColor};
	border-radius: ${defaultBorderRadius};
`;

export const RemoveGalleryImageButton = styled.button`
	width: 50%;
	background-color: ${redColor};
	border-radius: ${defaultBorderRadius};
`;

export const TextAreaContainer = styled.div`
	& .ql-toolbar {
		border-radius: ${defaultBorderRadius} ${defaultBorderRadius} 0 0;
	}

	& .ql-container {
		border-radius: 0 0 ${defaultBorderRadius} ${defaultBorderRadius};
	}

	//margin-bottom: ${defaultElementBottomMargin};
`;

export const FieldContainer = styled.div`
	display: flex;
	flex-direction: column;
	width: 60%;
	flex-grow: 1;
	gap: ${defaultElementsGap};
`;

export const DateContainer = styled.div`
	display: flex;
	gap: ${defaultElementsGap};
`;
