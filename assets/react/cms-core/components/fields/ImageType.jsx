import React, {useState} from "react";
import {InputLabelKassua, ObjectForm, TextFieldKassua} from "../../styles/formStyles";
import ReactImageUploading from "react-images-uploading";
import {
    GalleryButtons,
    UploadButton,
    RemoveButton,
    GalleryImagesContainer,
    GalleryContainer,
    GalleryImagesInnerContainer,
    GalleryImageContainer,
    GalleryImageInnerContainer,
    GalleryImageButtonContainer,
    UpdateGalleryImageButton, RemoveGalleryImageButton, FieldContainer
} from "./styles";

function ImageTypeFunction({field, inputRef})
{
    const [tfValue, setTFValue] = useState(field.value ? field.value : '');
    const [images, setImages] = React.useState(() => {
            const defaultFields = typeof field.value?.images === 'object' ? Object.values(field.value?.images) : [];

            return defaultFields;
        });

    const onChange = (imageList, addUpdateIndex) => {
        // data for submit
        setImages(imageList);
    };

    return (
        <FieldContainer>
            {field.label && (
                <InputLabelKassua labelfor={field.name}>
                    {field.label}
                </InputLabelKassua>
            )}
            <ReactImageUploading
                multiple
                value={images}
                onChange={onChange}
                dataURLKey="data_url"
            >
                {({
                      imageList,
                      onImageUpload,
                      onImageRemoveAll,
                      onImageUpdate,
                      onImageRemove,
                      isDragging,
                      dragProps,
                  }) => (
                    // write your building UI
                    <GalleryContainer className="upload__image-wrapper">
                        <GalleryButtons>
                            <UploadButton
                                style={isDragging ? {color: 'red'} : undefined}
                                onClick={(e) => {
                                    e.preventDefault();
                                    onImageUpload();
                                }}
                                {...dragProps}
                            >
                                Click or Drop here
                            </UploadButton>
                            <RemoveButton onClick={(e) => {
                                e.preventDefault();
                                onImageRemoveAll();
                            }}>Remove all images
                            </RemoveButton>
                        </GalleryButtons>
                        <GalleryImagesContainer>
                            <GalleryImagesInnerContainer>
                                {imageList.map((image, index) => (
                                    <GalleryImageContainer key={index} className="image-item">
                                        <GalleryImageInnerContainer>
                                            <img src={image['data_url']} alt="" width="100"/>
                                        </GalleryImageInnerContainer>
                                        <GalleryImageButtonContainer className="image-item__btn-wrapper">
                                            <UpdateGalleryImageButton onClick={(e) => {
                                                e.preventDefault();
                                                onImageUpdate(index);
                                            }}>Update
                                            </UpdateGalleryImageButton>
                                            <RemoveGalleryImageButton onClick={(e) => {
                                                e.preventDefault();
                                                onImageRemove(index);
                                            }}>Remove
                                            </RemoveGalleryImageButton>
                                        </GalleryImageButtonContainer>
                                    </GalleryImageContainer>
                                ))}
                            </GalleryImagesInnerContainer>
                        </GalleryImagesContainer>
                    </GalleryContainer>
                )}
            </ReactImageUploading>
            {images.map((image, index) => {
                const value = {
                    dataUrl: image.data_url,
                    file: {
                        name: image.file.name
                    }
                };

                return (
                    <input type={'hidden'} key={field.full_name+'['+index+']'} name={field.full_name+'['+index+']'} value={JSON.stringify(value)}/>
                );
            })}
        </FieldContainer>
    );
}

class ImageType extends React.Component {
    constructor() {
        super();
    }

    render() {
        const field = this.props.field;
        const inputRef = this.props.inputRef;

        return <ImageTypeFunction field={field} inputRef={inputRef}></ImageTypeFunction>
    }
}

export default ImageType;
