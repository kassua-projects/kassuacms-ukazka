import React, {useState} from "react";
import {InputLabelKassua, TextFieldKassua} from "../../styles/formStyles";
import {DateContainer, FieldContainer, LabelWidthItemContainer} from "./styles";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import SelectType from "./SelectType";
import {DatePicker} from "@mui/x-date-pickers";
import dayjs from "dayjs";

function DateTypeFunction({field}) {
    const [dateValue, setDateValue] = useState(dayjs(field.vars.value));
    return (
        <FieldContainer>
            {field.vars.label && (
                <InputLabelKassua labelfor={field.vars.name}>
                    {field.vars.label}
                </InputLabelKassua>
            )}
            <DatePicker
                value={dateValue}
                onChange={(newValue) => setDateValue(newValue)}
            ></DatePicker>
            <DateContainer>
            {field.children.map((dateField) => {
                return <SelectType key={dateField.vars.full_name} field={dateField.vars}></SelectType>
            })}
            </DateContainer>
            <input type={'hidden'} name={field.vars.full_name} value={dayjs(dateValue).format('YYYY-MM-DD')}/>
        </FieldContainer>
    );
}

class DateType extends React.Component {
    constructor() {
        super();
    }

    render() {
        const field = this.props.field;

        return <DateTypeFunction field={field}></DateTypeFunction>
    }
}

export default DateType;
