import React, {Component} from 'react';
import InstagramIcon from "@mui/icons-material/Instagram";
import {DoDisturbAlt} from "@mui/icons-material";
import EditIcon from '@mui/icons-material/Edit';
import DoneIcon from '@mui/icons-material/Done';
import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';
import AccountBalanceIcon from '@mui/icons-material/AccountBalance';
import BlurCircularIcon from '@mui/icons-material/BlurCircular';
import WebIcon from '@mui/icons-material/Web';
import RestaurantMenuIcon from '@mui/icons-material/RestaurantMenu';
import WysiwygIcon from '@mui/icons-material/Wysiwyg';
import ClassIcon from '@mui/icons-material/Class';
import SupervisedUserCircleIcon from '@mui/icons-material/SupervisedUserCircle';

class Icon extends React.Component {
    constructor() {
        super();
    }

    render() {
        switch (this.props.type) {
            case 'instagram':
                return <InstagramIcon></InstagramIcon>;
                break;
            case 'edit':
                return <EditIcon></EditIcon>
                break;
            case 'save':
                return <DoneIcon></DoneIcon>
                break;
            case 'add':
                return <AddIcon></AddIcon>
                break;
            case 'remove':
                return <DeleteIcon></DeleteIcon>
                break;
            case 'references':
                return <AccountBalanceIcon></AccountBalanceIcon>
                break;
            case 'dashboard':
                return <BlurCircularIcon></BlurCircularIcon>
                break;
            case 'web':
                return <WebIcon></WebIcon>
            case 'restaurant':
                return <RestaurantMenuIcon></RestaurantMenuIcon>
            case 'post':
                return <WysiwygIcon></WysiwygIcon>
            case 'category':
                return <ClassIcon></ClassIcon>
            case 'users':
                return <SupervisedUserCircleIcon></SupervisedUserCircleIcon>;
            default:
                return <DoDisturbAlt></DoDisturbAlt>
                break;
        }
    }
}

export default Icon;
