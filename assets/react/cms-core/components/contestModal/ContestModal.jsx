import React from "react";
import {GlobalStyle, Modal, Overlay} from "./styles";
import {DefaultActionButtonLink, PrimaryColorActionButton} from "../../controllers/styles";

function ContestModalFunction({handleShowModal, href}){
    return (
        <>
            <GlobalStyle></GlobalStyle>
            <Overlay>
                <Modal>
                    <h1>Potvrzení</h1>
                    <p>Opravdu chcete položku odstranit? V případě smazání již data nepůjde obnovit.</p>
                    <div>
                        <PrimaryColorActionButton onClick={handleShowModal}>Zrušit</PrimaryColorActionButton>
                        <DefaultActionButtonLink href={href}>Odstranit</DefaultActionButtonLink>
                    </div>
                </Modal>
            </Overlay>
        </>
    );
}

class ContestModal extends React.Component {
    constructor() {
        super();
    }

    render() {
        const props = this.props;

        return (<ContestModalFunction handleShowModal={props.handleShowModal} href={props.href}></ContestModalFunction>);
    }
}

export default ContestModal;
