import styled, { createGlobalStyle } from 'styled-components';
import {
	defaultBorderRadius, defaultBoxShadow,
	fontSize2,
	fontSize3,
	greyColor,
	primaryColor, secondaryColor, bigBorderRadius
} from "../../controllers/styles";

export const GlobalStyle = createGlobalStyle`
	html {
		overflow: hidden;
	}
`;

export const Overlay = styled.div`
	position: fixed;
	width: 100vw;
	height: 100vh;
	background-color: rgba(0, 0, 0, 0.7);
	//backdrop-filter: blur(6px);
	left: 0;
	top: 0;
	z-index: 9600;
	display: flex;
	justify-content: center;
	align-items: center;
`;

export const Modal = styled.div`
	background-color: #FFFFFF;
	border-radius: ${bigBorderRadius};
	max-width: 400px;
	overflow: hidden;
	margin: 1rem;
	box-shadow: ${defaultBoxShadow};
	border: 5px solid ${greyColor};

	& > h1 {
		font-size: ${fontSize2};
		font-weight: 700;
		line-height: 1;
		padding: 1rem 1rem 0 1rem;
	}
	& > p {
		font-size: ${fontSize3};
		padding: 0.5rem 1rem 1rem 1rem;
	}
	& > div {
		padding: 0.5rem 1rem 3px 1rem;
		background-color: ${greyColor};
		display: flex;
		justify-content: end;
		font-size: ${fontSize3};
		//font-weight: 700;
		line-height: 1;
		gap: 0.5rem;

		& > button, & > a {
			border-radius: ${defaultBorderRadius};
			background-color: ${primaryColor};
			padding: 0.5rem 1rem;
		}

		& > a {
			color: #FFFFFF;
			background-color: ${secondaryColor};
		}
	}
`;
