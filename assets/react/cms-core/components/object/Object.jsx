import React from "react";
import Icon from "../../components/icon/Icon";
import {InputLabelKassua, ObjectForm, TextFieldKassua} from "../../styles/formStyles";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import {ActionLinkSaveForm} from "./styles";
import TextareaType from "../fields/TextareaType";
import TextType from "../fields/TextType";
import ImageType from "../fields/ImageType";
import CollectionType from "../fields/CollectionType";
import SelectType from "../fields/SelectType";
import {ComponentTitle, DefaultActionButton, PrimaryColorActionButtonLink} from "../../controllers/styles";
import ObjectFields from "./ObjectFields";

class Object extends React.Component {
    constructor() {
        super();
    }

    render() {
        const props = this.props.backendProps;
        const adminProps = this.props.adminProps;
        const form = props['object']['form'];
        console.log(form);
        const fields = form['children'];

        return (
            <ObjectForm name={form.vars.name} method={form.vars.method} action={form.vars.action}>
                <div className={"objectInner"}>
                    <ComponentTitle>{adminProps.title}</ComponentTitle>
                    <div className={"objectInnerScroll"}>
                        {form.vars.label && (
                            <h2>{form.vars.label}</h2>
                        )}
                        <ObjectFields fields={fields}></ObjectFields>
                    </div>
                </div>
                <div className={"objectControl"}>
                    <DefaultActionButton type={"submit"}>Uložit</DefaultActionButton>
                    {/*<PrimaryColorActionButtonLink title={"test"} href={"/"} target={"_blank"}>Otevřít stránku v nové*/}
                    {/*    kartě</PrimaryColorActionButtonLink>*/}
                </div>
            </ObjectForm>
        );
    }
}

export default Object;
