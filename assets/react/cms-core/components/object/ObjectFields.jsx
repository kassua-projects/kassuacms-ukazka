import React from "react";
import TextareaType from "../fields/TextareaType";
import TextType from "../fields/TextType";
import SelectType from "../fields/SelectType";
import ImageType from "../fields/ImageType";
import CollectionType from "../fields/CollectionType";
import DateType from "../fields/DateType";
import NumberType from "../fields/NumberType";

class ObjectFields extends React.Component {
    constructor() {
        super();
    }

    render() {
        const fields = this.props.fields;
        // console.log(fields);
        return (
            <>
                {
                    fields.map((field, index) => {
                        switch (field.vars.block_prefixes[1]) {
                            case 'number':
                            case 'text':
                                if (field.vars.block_prefixes[2] === 'collection_entry')
                                {
                                    field.vars.block_prefixes = field.vars.block_prefixes.filter((currentValue, i) => {
                                        return i !== 2;
                                    });
                                    return (
                                        <ObjectFields key={field.vars.full_name} fields={[field]}></ObjectFields>
                                    );
                                }

                                return (
                                    <TextType key={field.vars.full_name} field={field.vars}></TextType>
                                );
                            case 'hidden':
                                return (
                                    <input key={field.vars.full_name} value={field.vars.value}
                                           name={field.vars.full_name} type={'hidden'}/>
                                );
                            case 'collection_entry':
                                if (field.children.length === 0) {
                                    field.vars.block_prefixes = field.vars.block_prefixes.filter((currentValue, i) => {
                                        return i !== 1;
                                    });
                                    return (
                                        <ObjectFields key={field.vars.full_name} fields={[field]}></ObjectFields>
                                    );
                                }

                                return (
                                    <ObjectFields key={field.vars.full_name} fields={field.children}></ObjectFields>
                                );
                            case 'collection':
                                return (
                                    <CollectionType key={field.vars.full_name} collectionField={field}></CollectionType>
                                );
                            case 'choice':
                                return (
                                    <SelectType key={field.vars.full_name} field={field.vars}></SelectType>
                                );
                            case 'date':
                                return (
                                    <DateType key={field.vars.full_name} field={field}></DateType>
                                );
                            // case 'number':
                            //     return (
                            //         <NumberType field={field}></NumberType>
                            //     );
                            case 'gallery':
                                return (
                                    <ImageType key={field.vars.full_name} field={field.vars}></ImageType>
                                );
                        }
                    })
                }
            </>
        );
    }
}

export default ObjectFields;
