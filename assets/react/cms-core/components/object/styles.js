import styled from "styled-components";
import {ActionLinkCSS} from "../../controllers/styles";
import {desktopMin} from "../../const/breakpoints";

export const ActionLinkSaveForm = styled.button`
	${ActionLinkCSS};

	// @media only screen and(min-width: ${desktopMin}px) {
	// 	display: none !important;
	// }
}
`;
