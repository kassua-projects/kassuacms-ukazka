import React, {useState} from "react";
import {
    ActionLinkContainer,
    KassuaDataTable,
    ActionLink, DataTableContainer, DataTableHeaderContainer, KassuaDataTableContainer
} from "./styles";
import Icon from "../../components/icon/Icon";
import ContestModal from "../contestModal/ContestModal";
import {DefaultActionButtonLink} from "../../controllers/styles";

function CollectionFunction({props}) {
    const [showModal, setShowModal] = useState(false);
    const [modalData, setModalData] = useState({});

    function handleShowModal(href)
    {
        setModalData({href: href});
        setShowModal(!showModal);
    }

    const columns = props.dataTable.columns;

    const actionCell = (row, index, column, id) => {
        const actionData = column.selector(row);
        if (!actionData) return;
        if (!actionData.type) return actionData;

        switch (actionData.type) {
            case 'link':
                return (
                    <ActionLinkContainer>
                        {
                            actionData.data.map((link, index) => {
                                return (
                                    <ActionLink onClick={(e) => {
                                        if (link.type == 'remove') {
                                            e.preventDefault();
                                            handleShowModal(link.href);
                                        }
                                    }} key={index} href={link.href} title={link.title} type={link.type} target={link.target}>
                                        <Icon type={link.type}></Icon>
                                    </ActionLink>
                                );
                            })
                        }
                    </ActionLinkContainer>
                );
            default:
                return actionData;
        }
    };

    columns.forEach((element, index) => {
        const sel = columns[index].selector.toString();
        columns[index] = {
            ...columns[index],
            selector: row => row[sel]
        };

        if (sel == 'action') {
            columns[index] = {
                ...columns[index],
                cell: (row, index, column, id) => actionCell(row, index, column, id),
            }
        }
    });

    return (
        <DataTableContainer>
            {showModal && (
                <ContestModal handleShowModal={handleShowModal} href={modalData.href}></ContestModal>
            )}
            <DataTableHeaderContainer>
            {props.actionLink && (
                <DefaultActionButtonLink href={props.actionLink.link} title={props.actionLink.title}>
                    {props.actionLink.title}
                </DefaultActionButtonLink>
            )}
            </DataTableHeaderContainer>
            <KassuaDataTableContainer>
                <KassuaDataTable
                    columns={columns}
                    data={props.dataTable.data}
                    selectableRows={props.dataTable.selectableRows}
                    pagination={true}
                />
            </KassuaDataTableContainer>
        </DataTableContainer>
    );
}

class Collection extends React.Component {
    constructor() {
        super();
    }

    render() {
        const props = this.props.backendProps;

        return (
            <CollectionFunction props={props}></CollectionFunction>
        )
    }
}

export default Collection;
