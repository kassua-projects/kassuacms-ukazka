import styled from "styled-components";
import {
	defaultBorderRadius,
	defaultPaddingFromSide,
	fontSize1,
	fontSize2,
	fontSize3,
	fontSize4,
	greyColor
} from "../../controllers/styles";
import {desktopMin} from "../../const/breakpoints"
import DataTable from "react-data-table-component";

export const ColumnContainer = styled.div`
	display: flex;
	padding: 0 15px;
	background-color: ${greyColor};
	height: 50px;
	align-items: center;

	@media only screen and (min-width: ${desktopMin}px) {
		padding: 0 20px;
		height: 75px;
	}
`;

export const ActionContainer = styled(ColumnContainer)`

`;

export const TitleContainer = styled(ColumnContainer)`
	font-size: ${fontSize2};
	font-weight: 800;

	@media only screen and (min-width: ${desktopMin}px) {
		font-size: ${fontSize1};
	}
`;

export const CollectionTable = styled.table`
	border-collapse: collapse;

	& th, & td {
		text-align: left;
		width: 100%;
		line-height: 1;
		padding: 0;
		border: 0;
	}

	& > tr {
		& > th {
			padding-bottom: 10px;
		}

		& > td {
			overflow: hidden;
			margin-bottom: 26px;

			&:first-child {
				border-radius: ${defaultBorderRadius} 0 0 ${defaultBorderRadius};
			}

			&:last-child {
				border-radius: 0 ${defaultBorderRadius} ${defaultBorderRadius} 0;
			}
		}

		&:last-child > td {
			margin-bottom: 0;
		}
	}
`;

export const KassuaDataTable = styled(DataTable)`
	& .rdt_TableHeadRow {
		font-size: ${fontSize3};
		font-weight: 800;
		background-color: ${greyColor};
		border-radius: ${defaultBorderRadius} ${defaultBorderRadius} 0 0;
		overflow: hidden;
		min-height: unset;
		padding: 0.7rem 0;

		@media only screen and (min-width: ${desktopMin}px) {
			font-size: ${fontSize3};
		}
	}

	& .rdt_TableRow {
		font-size: ${fontSize4};
		//font-weight: 800;

		@media only screen and (min-width: ${desktopMin}px) {
			font-size: ${fontSize4};
		}
	}
`;

export const KassuaDataTableContainer = styled.div`
	& .rdt_Pagination {
		border-radius: 0 0 ${defaultBorderRadius} ${defaultBorderRadius};
		min-height: unset;
		padding: 0.5rem 0;
	}
`;

export const ActionLink = styled.a`

`;

export const ActionLinkContainer = styled.div`
`;

export const DataTableContainer = styled.div`
	padding: calc(${defaultPaddingFromSide} / 2) ${defaultPaddingFromSide};
	display: flex;
	flex-direction: column;
	overflow-y: scroll;
	gap: 7px;
`;

export const DataTableHeaderContainer = styled.div`
	display: flex;
`;

