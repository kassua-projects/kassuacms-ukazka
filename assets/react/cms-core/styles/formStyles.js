import styled from "styled-components";
import {
	paragraphFont,
	defaultBorderRadius,
	greyColor,
	fontSize2,
	defaultPaddingFromSide,
	fontSize3
} from "../controllers/styles";
import {TextField} from "@mui/material";
import InputLabel from "@mui/material/InputLabel";

export const labelWidth = 'clamp(0px, 100%, 400px)';
export const defaultElementBottomMargin = '20px';
export const defaultElementsGap = '7px';

export const ObjectForm = styled.form`
	display: flex;
	width: 100%;
	height: inherit;

	& > .objectInner {
		display: flex;
		width: calc(100% - (300px - ${defaultPaddingFromSide}));
		flex-direction: column;
	}

	& > .objectInner > .objectInnerScroll {
		display: flex;
		width: 100%;
		flex-direction: row;
		flex-wrap: wrap;
		gap: calc(${defaultElementsGap} * 4);
		overflow-y: scroll;
		padding: calc(${defaultPaddingFromSide} / 3) ${defaultPaddingFromSide} ${defaultPaddingFromSide} ${defaultPaddingFromSide};
		& h2{
			font-size: ${fontSize2};
			width: 100%;
		}
	}

	& > .objectControl {
		display: flex;
		width: calc(300px - ${defaultPaddingFromSide});
		flex-shrink: 0;
		flex-direction: column;
		background-color: ${greyColor};
		height: 100%;
		padding: 10px;
	}

	& .MuiInputBase-root {
		font-family: ${paragraphFont} !important;
		border-radius: ${defaultBorderRadius};
		//margin-bottom: ${defaultElementBottomMargin};
		//&:last-child {
		//	margin-bottom: 0;
		//}
	}
`;

export const TextFieldKassua = styled(TextField)`

`;

export const InputLabelKassua = styled(InputLabel)`
	//width: ${labelWidth};
	//height: 48px;
	width: 100%;
	padding: 6px 12px !important;
	display: flex;
	margin-right: auto;
	background-color: ${greyColor};
	color: #000000 !important;
	font-weight: 800 !important;
	font-family: ${paragraphFont} !important;
	border-radius: ${defaultBorderRadius};
	font-size: ${fontSize3} !important;
	overflow: initial !important;
`;
