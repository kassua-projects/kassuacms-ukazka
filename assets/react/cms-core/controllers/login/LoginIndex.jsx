import React, {Component, useState} from 'react';
import {HeaderLogin, Title, LoginError, InnerContainerLogin, BottomBarLogin, MainContainerLogin} from './styles.js';
import {MainContainer, InnerContainer, Image, Form, PasswordToggle, LinkBack} from '../styles';

function LoginForm({backendProps, adminProps}) {
    const [passwordShown, setPasswordShown] = useState(false);
    const [inputs, setInputs] = useState({['_username']: backendProps["username"]});

    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({...values, [name]: value}))
    }

    const handleSubmit = (event) => {
        // event.preventDefault();
        // console.log(inputs);
    }

    const togglePassword = (e) => {
        e.preventDefault();
        setPasswordShown(!passwordShown);
    };

    return (
        <Form onSubmit={handleSubmit} action={backendProps["loginAction"]} method={"post"}>
            <button type="submit">Přihlásit</button>
            <div>
                <input type="text" id="username" name="_username" value={inputs._username || ""}
                       onChange={handleChange} placeholder={"E-mail"}/>
            </div>
            <div>
                <input type={passwordShown ? "text" : "password"} id="password" name="_password"
                       value={inputs._password || ""}
                       onChange={handleChange} placeholder={"Heslo"}/>
                <PasswordToggle passwordToggled={passwordShown} onClick={togglePassword}></PasswordToggle>
            </div>
            <LoginError>{backendProps["error"]}</LoginError>
            <input type="hidden" name="_target_path" value={adminProps.dashboardLink}/>
        </Form>
    );
}


export default class extends Component {
    render() {
        const t = this.props.backendProps;
        const adminProps = this.props.adminProps;

        return (
            <MainContainerLogin>
                <HeaderLogin><Image src={"/bundles/kassuacmscore/images/logo-w.svg"}
                                    alt={"Logo Kassua Studio"}/></HeaderLogin>
                <InnerContainerLogin>
                    <Title>{t["title"]}</Title>
                    <LoginForm backendProps={t} adminProps={adminProps}></LoginForm>
                </InnerContainerLogin>
                <BottomBarLogin>
                    <Image src={"/bundles/kassuacmscore/images/icons/left.png"}/>
                    <LinkBack href={adminProps.webLink}>Zpět na web</LinkBack>
                </BottomBarLogin>
            </MainContainerLogin>
        );
    }
}
