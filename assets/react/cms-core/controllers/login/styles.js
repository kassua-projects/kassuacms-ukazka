import styled from "styled-components";
import {
	menuHeight,
	Header,
	InnerContainer,
	fontSizeTitle,
	BottomBar,
	MainContainer,
	subMenuHeight,
	defaultPaddingFromSide,
	secondaryColor
} from "../styles";
import {desktopMin} from "../../const/breakpoints"

export const HeaderLogin = styled(Header)`
	justify-content: center;
`;

export const InnerContainerLogin = styled(InnerContainer)`
	//height: calc(100vh - (${subMenuHeight} + (${menuHeight} * 2)));
	justify-content: center;
	align-items: center;

	@media only screen and (min-width: ${desktopMin}px) {
		height: calc(100vh - (${menuHeight} + ${menuHeight} + ${defaultPaddingFromSide}));
	}
`;

export const Title = styled.h1`
	font-weight: bold;
	font-size: ${fontSizeTitle};
	line-height: 1;
	text-align: center;
	margin-bottom: 20px;
`;

export const LoginError = styled.span`
	width: 100%;
	text-align: center;
	color: red;
`;

export const BottomBarLogin = styled(BottomBar)`
	@media only screen and (min-width: ${desktopMin}px) {
		display: flex;
	}
`;

export const MainContainerLogin = styled(MainContainer)`
	padding-top: ${menuHeight} !important;
	padding-bottom: ${menuHeight} !important;
	// background: linear-gradient(-45deg, #FFFFFF60, ${secondaryColor}60);
	// animation: gradient 15s ease infinite;
	// background-size: 400% 400%;
	// @keyframes gradient {
	// 	0% {
	// 		background-position: 0% 50%;
	// 	}
	// 	50% {
	// 		background-position: 100% 50%;
	// 	}
	// 	100% {
	// 		background-position: 0% 50%;
	// 	}
	// }
`;
