import React, {Component} from "react";
import Layout from "../Layout";
import {InnerSectionContainer} from "../styles";
import {ToDoContainer} from "./styles";

export default class extends Component {
    constructor() {
        super();
    }

    render() {
        const props = this.props.backendProps;

        return (
            <Layout adminProps={this.props.adminProps} backendProps={props} title={"Object"}>
                <InnerSectionContainer>
                    <ToDoContainer>
                        <h1>Tato funkcionalita se teprve připravuje!</h1>
                    </ToDoContainer>
                </InnerSectionContainer>
            </Layout>
        )
    };
}
