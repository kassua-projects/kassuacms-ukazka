import styled from "styled-components";
import {defaultPaddingFromSide} from "../styles";

export const ToDoContainer = styled.div`
	padding: ${defaultPaddingFromSide};
`;
