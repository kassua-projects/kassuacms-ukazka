import React, {Component} from 'react';
import Layout from '../Layout';
import Collection from '../../components/collection/Collection';
import {ActionLink, InnerSectionContainer} from '../styles';
import Icon from "../../components/icon/Icon";

export default class extends Component {
    constructor() {
        super();
    }

    render() {
        const props = this.props.backendProps;

        return (
            <Layout adminProps={this.props.adminProps} backendProps={props} title={"Collection"}>
                <InnerSectionContainer>
                    <Collection backendProps={props}/>
                    {/*<ActionLink href={props.actionLink.link} title={props.actionLink.title}>*/}
                    {/*    <Icon type={"add"}></Icon>*/}
                    {/*</ActionLink>*/}
                </InnerSectionContainer>
            </Layout>
        )
    };
}
