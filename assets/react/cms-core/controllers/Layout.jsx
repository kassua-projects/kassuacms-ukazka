import React, {useState, useEffect} from "react"
import {
    MainContainer,
    Header,
    InnerContainer,
    Image,
    MenuButton,
    LinkBack,
    BottomBar,
    SubHeader,
    LeftMenu,
    LeftMenuLink,
    GlobalStyle,
    UserPopUp,
    LeftMenuHeading,
    LeftMenuFooter,
    SubHeaderTitle,
    MenuLink, ComponentContainer, ComponentTitle
} from "./styles";
import {mobileMax} from '../const/breakpoints'
import Icon from "../components/icon/Icon";
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import 'dayjs/locale/cs';

function LayoutFunc({props}){
    const adminProps = props.adminProps;
    const backendProps = props.backendProps;
    // console.log(props);

    const [windowSize, setWindowSize] = useState(getWindowSize());
    const [isMobile, setIsMobile] = useState(windowSize.innerWidth <= mobileMax);

    useEffect(() => {
        function handleWindowResize() {
            setWindowSize(getWindowSize());
            setIsMobile(getWindowSize().innerWidth <= mobileMax);
        }

        window.addEventListener('resize', handleWindowResize);

        return () => {
            window.removeEventListener('resize', handleWindowResize);
        };
    }, []);

    function getWindowSize() {
        const {innerWidth, innerHeight} = window;
        return {innerWidth, innerHeight};
    }

    const [menuShown, setMenuShown] = useState(()=> {
        if (isMobile)
            return false;

        const storageMenuShown = localStorage.getItem('menuShown');

        if (storageMenuShown !== null && storageMenuShown == 'false')
            return false;

        return !isMobile;
    });
    const [userShown, setUserShown] = useState(false);

    const toggleMenu = (e) => {
        e.preventDefault();
        if (isMobile) setUserShown(false);
        localStorage.setItem('menuShown', !menuShown);
        setMenuShown(!menuShown);
    };

    const toggleUser = (e) => {
        e.preventDefault();
        if (isMobile) setMenuShown(false);
        setUserShown(!userShown);
    };

    return (
        <MainContainer>
            <GlobalStyle overflowHidden={isMobile && (menuShown || userShown)}></GlobalStyle>
            <Header>
                <MenuButton onClick={toggleMenu}>
                    <Image src={"/bundles/kassuacmscore/images/icons/menu-2.png"} alt={"Menu icon"}/>
                </MenuButton>
                <Image src={"/bundles/kassuacmscore/images/logo-w.svg"} alt={"Logo Kassua Studio"}/>
                <MenuButton onClick={toggleUser}>
                    <Image src={"/bundles/kassuacmscore/images/icons/user.png"} alt={"User icon"}/>
                </MenuButton>
            </Header>
            {adminProps?.primaryMenu && (
                <SubHeader>
                    <ul>
                        {adminProps.primaryMenu.items.map((item, index) => {
                            return (
                                <li key={index}>
                                    <MenuLink href={item.url} title={item.title} isActive={item.is_active}>{item.title}</MenuLink>
                                </li>
                            );
                        })}
                    </ul>
                </SubHeader>
            )}
            {adminProps?.secondaryMenu && (
                <LeftMenu menuShown={menuShown}>
                    {adminProps.secondaryMenu.title && (
                        <LeftMenuHeading>{adminProps.secondaryMenu.title}</LeftMenuHeading>
                    )}
                    <ul>
                        {adminProps.secondaryMenu.items.map((item, index) => {
                            return (
                                <li key={index}>
                                    <LeftMenuLink href={item.url} isActive={item.is_active}>
                                        {item.icon && (
                                            <Icon type={item.icon}></Icon>
                                        )}
                                        <span>{item.title}</span>
                                    </LeftMenuLink>
                                </li>
                            );
                        })}
                    </ul>
                    <LeftMenuFooter>© Kassua Studio 2023</LeftMenuFooter>
                </LeftMenu>
            )}
            <UserPopUp userShown={userShown}>
                <span>{adminProps.userName}</span>
                <a href={adminProps.profileLink}>Profil</a>
                <a href={adminProps.logoutLink}>Odhlásit se</a>
            </UserPopUp>
            <InnerContainer menuShown={menuShown}>
                <ComponentContainer>
                    <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale={"cs"}>
                        {props.children}
                    </LocalizationProvider>
                </ComponentContainer>
            </InnerContainer>
            <BottomBar>
                {/*<Image src={"/bundles/kassuacmscore/images/icons/left.png"} />*/}
                <span>{adminProps.title}</span>
            </BottomBar>
        </MainContainer>
    );
}

class Layout extends React.Component {
    constructor() {
        super();
    }

    render() {
        // const t = this.props.backendProps;
        return (
            <LayoutFunc props={this.props}></LayoutFunc>
        )
    }
}

export default Layout;
