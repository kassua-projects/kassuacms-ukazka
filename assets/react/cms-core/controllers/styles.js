import styled, {createGlobalStyle, css} from "styled-components";
import {desktopMin} from "../const/breakpoints"
import {darken} from "polished";

export const primaryColor = '#73BFAC';
export const secondaryColor = '#2275BA';
export const backgroundColor = '#FFFFFF';
export const lightGreyColor = '#F5F5F5';
export const greyColor = 'rgb(230,230,230)';
export const darkGreyColor = '#858585';
export const redColor = 'rgb(220, 70, 70)';
export const defaultPaddingFromSide = '30px';
export const defaultBorderRadius = '8px';
export const mediumBorderRadius = '12px';
export const bigBorderRadius = '16px';
export const menuHeight = '50px';
export const menuIconHeight = '22px';
export const desktopLeftMenuWidth = '300px';
export const subMenuHeight = '35px';
export const desktopSubMenuHeight = '40px';
export const font = 'Gilroy';
export const fontSizeTitle = '32px';
export const fontSize1 = '24px';
export const fontSize2 = '19px';
export const fontSize3 = '16px';
export const fontSize4 = '15px';
export const defaultTransition = '200ms';
export const elementMaxWidth = 'clamp(300px, 529px, 100%)';
export const titleFont = 'Gilroy';
export const paragraphFont = 'Gilroy';
export const defaultBoxShadow = '0 4px 8px 3px rgba(0,0,0,.15), 0 1px 3px rgba(0,0,0,.3)';

export const GlobalStyle = createGlobalStyle`
	::-webkit-scrollbar {
		height: 8px;
		//max-width: 8px;
	}
	::-webkit-scrollbar-corner {
		background: transparent;
	}
	::-webkit-scrollbar-thumb {
		background-color: rgba(0,0,0,.2);
		background-clip: padding-box;
		border: solid transparent;
		border-width: 1px 1px 1px 6px;
		min-height: 28px;
		//padding: 100px 0 0;
		//border-radius: ${defaultBorderRadius};
		-webkit-box-shadow: inset 1px 1px 0 rgba(0,0,0,.1), inset 0 -1px 0 rgba(0,0,0,.07);
		box-shadow: inset 1px 1px 0 rgba(0,0,0,.1), inset 0 -1px 0 rgba(0,0,0,.07);
	}

	body {
		overflow: ${props => props.overflowHidden ? 'hidden' : 'initial'};
		background-color: ${lightGreyColor};
	}

	.col1-10 {
		width: 5%!important;
	}
	.col2-10 {
		width: 15%!important;
	}
	.col3-10 {
		width: 25%!important;
	}
	.col4-10 {
		width: 35%!important;
	}
	.col5-10 {
		width: 45%!important;
	}
	.col6-10 {
		width: 55%!important;
	}
	.col7-10 {
		width: 65%!important;
	}
	.col8-10 {
		width: 75%!important;
	}
	.col9-10 {
		width: 85%!important;
	}
	.col10-10 {
		width: 95%!important;
	}
`

export const MainContainer = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
	height: 100%;
	min-height: 100vh;
	padding-top: calc(${menuHeight} + ${subMenuHeight});
	padding-bottom: ${menuHeight};

	@media only screen and (min-width: ${desktopMin}px) {
		padding-bottom: 0;
		padding-top: calc(${menuHeight} + ${desktopSubMenuHeight});
	}
`;

export const InnerContainer = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
	height: calc(100vh - (${subMenuHeight} + (${menuHeight} * 2)));
	flex-grow: 1;
	align-self: end;
	transition: ${defaultTransition};

	& h1 {
		font-size: ${fontSize1};
	}

	& p, & span {
		font-size: ${fontSize3};
	}

	@media only screen and (min-width: ${desktopMin}px) {
		padding: 0;
		margin: calc(${defaultPaddingFromSide} / 2) ${defaultPaddingFromSide} calc(${defaultPaddingFromSide} / 2) auto;
		height: calc(100vh - (${menuHeight} + ${desktopSubMenuHeight} + ${defaultPaddingFromSide}));
		width: calc(100% - ${props => props.menuShown ? `calc(${desktopLeftMenuWidth} + ${defaultPaddingFromSide})` : `calc(${defaultPaddingFromSide} * 2)`});
	}
`;

export const ComponentContainer = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
	height: 100%;
	background-color: #FFFFFF;
	overflow: hidden;

	@media only screen and (min-width: ${desktopMin}px) {
		border-radius: ${bigBorderRadius};
	}
`;

export const ComponentTitle = styled.h1`
	display: none;
	// margin-right: ${defaultPaddingFromSide};
	//margin-left: 4px;
	// color: ${secondaryColor};
	//font-weight: 800;
	padding: calc(${defaultPaddingFromSide} / 2) ${defaultPaddingFromSide};
	font-size: ${fontSize1} !important;
	line-height: 1;
	//margin-bottom: 5px;
	//margin-top: 8px;
	//background-color: #FFFFFF;

	@media only screen and (min-width: ${desktopMin}px) {
		display: flex;
	}
`;

export const Header = styled.header`
	display: flex;
	position: fixed;
	top: 0;
	left: 0;
	height: ${menuHeight};
	width: 100%;
	padding: 10px ${defaultPaddingFromSide};
	background-color: ${primaryColor};
	justify-content: space-between;
	align-items: center;
	z-index: 1000;
`;

export const SubHeader = styled.div`
	display: flex;
	position: fixed;
	left: 0;
	top: ${menuHeight};
	width: 100%;
	background-color: ${greyColor};
	align-items: center;
	height: ${subMenuHeight};
	z-index: 9000;
	overflow-x: touch;
	justify-content: space-between;
	//border-bottom: 10px solid ${lightGreyColor};

	& > ul {
		padding: 0 ${defaultPaddingFromSide};
		display: flex;
		height: 100%;

		& > li {
			//margin-right: 17px;
			display: flex;
			height: 100%;

			&:last-child {
				margin-right: 0;
			}

			& > a {
				font-size: ${fontSize3};
				//text-decoration: underline;
				//font-weight: 800;

				&:hover {
					//text-decoration: none;
				}
			}
		}
	}

	@media only screen and (min-width: ${desktopMin}px) {
		height: ${desktopSubMenuHeight};
	}
`;

export const SubHeaderTitle = styled.h1`
	display: none;
	margin-right: ${defaultPaddingFromSide};
	color: ${secondaryColor};
	font-weight: 800;
	font-size: ${fontSize2};

	@media only screen and (min-width: ${desktopMin}px) {
		display: flex;
	}
`;

export const LeftMenu = styled.div`
	position: fixed;
	display: flex;
	flex-direction: column;
	width: 100%;
	height: calc(100% - ${menuHeight});
	top: ${menuHeight};
	z-index: 9500;
	transition: ${defaultTransition};
	left: ${props => props.menuShown ? 0 : '-100%'};
	background-color: ${lightGreyColor};
	overflow-y: touch;

	& > ul {
		display: flex;
		flex-direction: column;
		border-top: 1px solid #FFFFFF;

		& > li {
			display: flex;
			//border-top: 1px solid #FFFFFF;

			&:last-child {

			}
		}
	}

	@media only screen and (min-width: ${desktopMin}px) {
		top: calc(${menuHeight} + ${desktopSubMenuHeight});
		height: calc(100% - ${menuHeight} - ${desktopSubMenuHeight});
		width: ${desktopLeftMenuWidth};
		left: ${props => props.menuShown ? 0 : '-' + desktopLeftMenuWidth};
	}
`;

export const LeftMenuLink = styled.a`
	display: flex;
	color: ${secondaryColor};
	font-size: ${fontSize3};
	//text-decoration: underline;
	//font-weight: 800;
	padding: 12px ${defaultPaddingFromSide};
	width: 100%;
	background-color: ${props => props.isActive ? '#FFFFFF' : 'transparent'};

	& > span {
		margin-left: 10px;
	}

	&:hover {
		${props => props.isActive ? '' : 'background-color: rgba(255,255,255,0.65);'};
		//text-decoration: none;
	}
`;

export const MenuLink = styled.a`
	display: flex;
	background-color: ${props => props.isActive ? lightGreyColor : 'transparent'};
	border-radius: ${mediumBorderRadius} ${mediumBorderRadius} 0 0;
	padding: 0 1rem;
	align-items: center;

	&:hover {
		${props => props.isActive ? '' : 'background-color: '+lightGreyColor+'a6;'};
	}
`;

export const LeftMenuHeading = styled.span`
	display: flex;
	font-weight: 800;
	font-size: ${fontSize3};
	padding: 24px ${defaultPaddingFromSide} 12px ${defaultPaddingFromSide};
	line-height: 1;

	@media only screen and (min-width: ${desktopMin}px) {
		padding: calc(${defaultPaddingFromSide} / 2) ${defaultPaddingFromSide} calc(${defaultPaddingFromSide} / 2) ${defaultPaddingFromSide};
	}
`;

export const LeftMenuFooter = styled.span`
	//font-weight: 800;
	font-size: ${fontSize3};
	padding: calc(${defaultPaddingFromSide} / 2) ${defaultPaddingFromSide} calc(${defaultPaddingFromSide} / 2) ${defaultPaddingFromSide};
	display: flex;
	margin-top: auto;
`;

export const BottomBar = styled.div`
	display: flex;
	height: ${menuHeight};
	width: 100%;
	padding: 15px ${defaultPaddingFromSide};
	background-color: ${primaryColor};
	position: fixed;
	bottom: 0;
	left: 0;
	font-size: ${fontSize3};
	color: #FFFFFF;
	font-weight: bold;
	text-transform: uppercase;
	z-index: 9000;

	& > img {
		margin: auto 10px auto 0;
		height: 16px;
	}

	& > * {
		line-height: calc(${menuHeight} - 30px);
		height: calc(${menuHeight} - 30px);
		vertical-align: middle;
		display: flex;
	}

	@media only screen and (min-width: ${desktopMin}px) {
		display: none;
	}
`;

export const LinkBack = styled.a`
`;

export const Image = styled.img`
	height: 100%;
`;

export const Form = styled.form`
	display: flex;
	flex-direction: column;
	width: ${elementMaxWidth};

	& input {
		width: 100%;
		height: 48px;
		background-color: ${lightGreyColor};
		border-radius: ${defaultBorderRadius};
		font-size: ${fontSize2};
		padding: 0 10px;
		color: #000000;
		margin-bottom: 17px;
		order: 1;

		&:last-child {
			margin-bottom: 10px;
		}

		&:focus, &:active {
			border: unset;
			outline: 1px solid ${secondaryColor};
		}

		&::placeholder {
			color: ${darkGreyColor};
		}
	}

	& > div {
		width: 100%;
		display: flex;
		position: relative;
	}

	& > button[type="submit"] {
		width: auto;
		margin: 10px auto 0 auto;
		font-size: 20px;
		font-weight: bold;
		background-color: ${primaryColor};
		color: #FFFFFF;
		border-radius: ${defaultBorderRadius};
		padding: 5px 30px;
		order: 2;

		&:hover, &:focus, &:active {
			background-color: ${secondaryColor};
		}
	}
`;

export const PasswordToggle = styled.button`
	position: absolute;
	background-image: url("/bundles/kassuacmscore/images/icons/${props => props.passwordToggled ? "hide.png" : "show.png"}");
	background-color: ${greyColor};
	//background-size: contain;
	background-position: center;
	background-repeat: no-repeat;
	background-size: 24px;
	right: 0;
	width: 48px;
	border-radius: ${defaultBorderRadius};
	height: 48px;
`;

export const MenuButton = styled.button`
	height: ${menuIconHeight};
`;

export const InfoPanel = styled.div`
	display: flex;
	width: 100%;
	background-color: ${greyColor};
	justify-content: center;
	border-radius: ${defaultBorderRadius};
	align-items: center;
	font-weight: bold;
	padding: 6px;
	font-size: ${fontSize2};

	& > div {
		background-color: ${primaryColor};
		border-radius: ${defaultBorderRadius};
		padding: 8px;
		margin-right: 16px;

		& > img {
			height: 16px;
		}
	}
`;

export const Title = styled.h1`
	font-size: ${fontSize1};
	font-weight: 800;
	line-height: 1;
	margin-bottom: 17px;
`;

export const FullWidthLabel = styled.label`
	width: 100%;
	font-size: ${fontSize2};
	font-weight: 800;
	padding: 12px;
	background-color: ${greyColor};
	border-radius: ${defaultBorderRadius};
`;

export const UserPopUp = styled.div`
	position: fixed;
	top: calc(${menuHeight} - 10px);
	right: ${defaultPaddingFromSide};
	background-color: #ffffff;
	background-clip: padding-box;
	border: 5px solid ${lightGreyColor};
	border-radius: ${defaultBorderRadius};
	display: ${props => props.userShown ? 'flex' : 'none'};
	z-index: 9400;
	padding: 17px;
	flex-direction: column;
	font-size: ${fontSize3};
	font-weight: 800;
	box-shadow: ${defaultBoxShadow};

	// &:before {
		//   top: calc(${menuHeight} - ((${menuHeight} - ${menuIconHeight}) / 2));
	//   right: 22px;
	//   content: "";
	//   display: flex;
	//   position: fixed;
	//   width: 0;
	//   height: 0;
		//   border-left: calc((${menuHeight} - ${menuIconHeight}) / 2) solid transparent;
		//   border-right: calc((${menuHeight} - ${menuIconHeight}) / 2) solid transparent;
		//   border-bottom: calc((${menuHeight} - ${menuIconHeight}) / 2) solid ${lightGreyColor};
	// }

	& > a {
		font-size: ${fontSize3};
		text-decoration: underline;
		font-weight: 200;

		&:hover {
			text-decoration: none;
		}
	}
`;

export const ActionLinkCSS = css`
	display: flex;
	width: ${menuHeight};
	height: ${menuHeight};
	background-color: ${primaryColor};
	position: fixed;
	bottom: 0;
	justify-content: center;
	align-items: center;
	z-index: 9001;
	right: 0;
	color: #FFFFFF;
	transition: ${defaultTransition};

	& > svg {
		font-size: 2.36rem;
	}

	@media only screen and (min-width: ${desktopMin}px) {
		bottom: ${defaultPaddingFromSide};
		background-color: ${secondaryColor};
		right: ${defaultPaddingFromSide};
		width: 60px;
		height: 60px;
		border-radius: 100%;
		box-shadow: ${defaultBoxShadow};

		& > svg {
			font-size: 2rem;
		}

		&:hover, &:focus {
			background-color: ${darken(0.10, secondaryColor)};
		}
	}
`;

export const ActionLink = styled.a`
	${ActionLinkCSS}
`;

export const InnerSectionContainer = styled.div`
	width: 100%;
	height: 100%;
	display: flex;
	flex-direction: column;
	//overflow-y: scroll;
	// padding: ${defaultPaddingFromSide} ${defaultPaddingFromSide};

	@media only screen and (min-width: ${desktopMin}px) {
		//padding: 0 90px;
	}
`;

export const DefaultButtonCSS = css`
	display: flex;
	line-height: 1;
	padding: 0.5rem 1rem;
	font-size: ${fontSize3};
	background-color: ${secondaryColor};
	color: #FFFFFF;
	border-radius: ${defaultBorderRadius};
	text-align: center;
	//margin-bottom: 7px;
	transition: ${defaultTransition};
	justify-content: center;

	&:hover{
		background-color: ${darken(0.10, secondaryColor)};
	}
`;

export const DefaultActionButtonLink = styled.a`
	${DefaultButtonCSS};
`;

export const DefaultActionButton = styled.button`
	${DefaultButtonCSS};
`;

export const PrimaryColorButtonCSS = css`
	display: flex;
	line-height: 1;
	padding: 0.5rem 1rem;
	font-size: ${fontSize3};
	background-color: ${primaryColor};
	color: #FFFFFF;
	border-radius: ${defaultBorderRadius};
	text-align: center;
	//margin-bottom: 7px;
	transition: ${defaultTransition};
	justify-content: center;

	&:hover{
		background-color: ${darken(0.10, primaryColor)};
	}
`;

export const PrimaryColorActionButtonLink = styled.a`
	${PrimaryColorButtonCSS};
`;

export const PrimaryColorActionButton = styled.button`
	${PrimaryColorButtonCSS};
`;
