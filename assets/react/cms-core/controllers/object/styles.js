import styled from "styled-components";
import {defaultPaddingFromSide} from "../styles";

export const ObjectInner = styled.div`
	padding: ${defaultPaddingFromSide};
	display: flex;
`;
