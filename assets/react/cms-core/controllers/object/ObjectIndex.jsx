import React, {Component} from "react";
import Layout from '../Layout';
import {ActionLink, InnerSectionContainer, Title, FullWidthLabel} from "../styles";
import Icon from "../../components/icon/Icon";
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, {SelectChangeEvent} from '@mui/material/Select';
import {TextFieldKassua, InputLabelKassua, ObjectForm} from "../../styles/formStyles";
import Object from "../../components/object/Object";

export default class extends Component {
    constructor() {
        super();
    }

    render() {
        const props = this.props.backendProps;

        return (
            <Layout adminProps={this.props.adminProps} backendProps={props} title={"Object"}>
                <InnerSectionContainer>
                    <Object backendProps={props} adminProps={this.props.adminProps}></Object>
                </InnerSectionContainer>
            </Layout>
        )
    };
}
