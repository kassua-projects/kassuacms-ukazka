import React, {Component} from 'react';
import Layout from '../Layout';
import {Image, InfoPanel, Title} from '../styles';
import {MainSection, InfoPanelDashboard, CallsFrom, DashboardBlock} from './styles';

export default class extends Component {
    constructor() {
        super();
    }

    render() {
        // console.log(this.props);
        return (
            <Layout adminProps={this.props.adminProps} backendProps={this.props.backendProps} title={"Dashboard"}>
                <DashboardBlock>
                    <MainSection>
                        <Title>Vítejte</Title>
                        <p>Pro snadnější a lepší užívání jsme
                            pro Vás vytvořili vlastní administrační
                            systém. V případě rady či zjištěné chyby
                            kontaktujte naší zákaznickou podporu.</p>
                    </MainSection>
                    <InfoPanelDashboard>
                        <div>
                            <Image src={"/bundles/kassuacmscore/images/icons/support.png"} alt={"Support icon"}/>
                        </div>
                        <h2>Zákaznická podpora</h2>
                    </InfoPanelDashboard>
                    <span><b>Email:</b> robert.smid@kassua.cz</span>
                    <span><b>Telefon:</b> +420 725 873 047</span>
                    <CallsFrom>
                        <span>Telefonické hovory přijímáme</span>
                        <span>od 10:00 do 17:00</span>
                    </CallsFrom>
                </DashboardBlock>
            </Layout>
        );
    }
}
