import styled from "styled-components";
import {InfoPanel, secondaryColor, elementMaxWidth, defaultPaddingFromSide} from "../styles";

export const MainSection = styled.div`
	margin-bottom: 48px;
`;

export const InfoPanelDashboard = styled(InfoPanel)`
	margin-bottom: 17px;
`;

export const CallsFrom = styled.div`
	display: flex;
	margin-top: 10px;
	flex-direction: column;

	& > span:nth-child(2) {
		color: ${secondaryColor};
	}
`;

export const DashboardBlock = styled.div`
	width: ${elementMaxWidth};
	padding: ${defaultPaddingFromSide} ${defaultPaddingFromSide};
	display: flex;
	align-self: center;
	flex-direction: column;
`;
